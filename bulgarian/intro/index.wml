#use wml::debian::template title="Въведение в Дебиан" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Дебиан е общност на съмишленици</h2>
      <p>Хиляди доброволци от цял свят работят заедно по операционната система
         Дебиан, обръщайки специално внимание на свободния софтуер.
         Запознайте се с проекта Дебиан.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Хората:</a>
          Кои сме ние и какво правим
        </li>
        <li>
          <a href="philosophy">Идеята:</a>
          Защо и как го правим
        </li>
        <li>
          <a href="../devel/join/">Присъединяване:</a>
          Можете да станете част от нас!
        </li>
        <li>
          <a href="help">Как можете да помогнете?</a>
        </li>
        <li>
          <a href="../social_contract">Обществен договор:</a>
          Нашите цели и мотиви
        </li>
        <li>
          <a href="diversity">Всеки е добре дошъл:</a>
          Декларация за многообразие
        </li>
        <li>
          <a href="../code_of_conduct">За участниците:</a>
          Кодекс за поведение
        </li>
        <li>
          <a href="../partners/">Партньори:</a>
          Организации, които помагат на проекта
        </li>
        <li>
          <a href="../donations">Дарения</a>
          Как да станете спонсор на проекта
        </li>
        <li>
          <a href="../legal/">Правна проблеми:</a>
          Лицензи, търговски марки, политика за поверителност, политика за
          патенти и др.
        </li>
        <li>
          <a href="../contact">Контакт:</a>
          Как да се свържете с нас
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id=software></a>
      <h2>Дебиан е операционна система</h2>
      <p>Дебиан е свободна операционна система, разработвана и поддържана от проекта Дебиан. Свободна дистрибуция, базирана на Линукс, с хиляди приложения, които да посрещнат нуждите на потребителите.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Изтегляне:</a>
          От къде да изтеглите Дебиан
        </li>
        <li>
          <a href="why_debian">Защо Дебиан:</a>
          Причини да изберете Дебиан
        </li>
        <li>
          <a href="../support">Поддръжка:</a>
          Помощ
        </li>
        <li>
          <a href="../security">Сигурност:</a>
          Последен бюлетин <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Софтуер:</a>
          Търсене и разглеждане на списъка със софтуерни пакети
        </li>
        <li>
          <a href="../doc">Документация</a>
          Ръководство за инсталиране, често задавани въпроси, уики и други
        </li>
        <li>
          <a href="../Bugs">Доклади за проблеми:</a>
          Как да съобщите за проблем, документация за системата за проследяване на проблеми
        </li>
        <li>
          <a href="https://lists.debian.org/">Пощенски списъци</a>
          Пощенски списъци за потребители, сътрудници и други
        </li>
        <li>
          <a href="../blends">Дестилати:</a>
          Групи пакети за специфични нужди
        </li>
        <li>
          <a href="../devel">За сътрудници:</a>
          Информация, предназначена предимно за сътрудниците на Дебиан
        </li>
        <li>
          <a href="../ports">Архитектури:</a>
          Поддържани процесорни архитектури
        </li>
        <li>
          <a href="search">Търсене:</a>
          Информация за използването на търсачката на Дебиан
        </li>
        <li>
          <a href="cn">Езици:</a>
          Информация за разглеждане на сайта на различен език
        </li>
      </ul>
    </div>
  </div>

</div>
