#use wml::debian::template title="Informações de lançamento do Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4a4923573595a8539cd9aca5189940d45bcbbb29"

<p>O Debian <current_release_bookworm> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 foi lançado inicialmente em <:=spokendate('2023-06-10'):>."
/>
O lançamento incluiu várias grandes mudanças descritas em
nosso <a href="$(HOME)/News/2023/20230610">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

#<p><strong>O Debian 12 foi substituído pelo
#<a href="../lenny/">Debian 13 (<q>trixie</q>)</a>.
#Atualizações de segurança foram descontinuadas em <:=spokendate('2026-06-10'):>.
#</strong></p>

### Este parágrafo é orientativo, revise antes de publicar!
#<p><strong>Contudo, o bookworm se beneficia do suporte de longo prazo (LTS -
#Long Term Support) até 30 de junho de 2028. O LTS é limitado ao i386, amd64,
#armel, armhf e arm64.
#Todas as outras arquiteturas não são mais suportadas no bookworm.
#Para mais informações, consulte a <a
#href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
#</strong></p>

<p>Para obter e instalar o Debian, consulte
a página de <a href="debian-installer/">informação de instalação</a> e o
<a href="installmanual">guia de instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, consulte as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

###  Ative o seguinte, quando o período LTS começar.
#<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Arquiteturas de computadores suportadas no lançamento inicial do bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Nós fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="../reportingbugs">relatar outros problemas</a>
para nós.</p>

<p>Por último mas não menos importante, nós temos uma lista de
<a href="credits">pessoas que recebem crédito</a> por fazerem este lançamento
acontecer.</p>
