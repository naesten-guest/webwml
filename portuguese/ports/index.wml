#use wml::debian::template title="Portes"
#use wml::debian::translation-check translation="e0652549b916d3585e3eed9d093a6711af721019"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="intro">Introdução</toc-add-entry>
<p>
 Como a maioria de vocês sabe, o <a href="https://www.kernel.org/">Linux</a>
 é apenas um kernel (núcleo). E durante muito tempo
 o kernel Linux funcionou somente nas máquinas da série x86 da Intel, desde
 o 386.
</p>
<p>
 No entanto, isso não é mais verdade, de forma alguma. O kernel Linux tem
 sido portado para uma grande e crescente lista de arquiteturas.
 E, seguindo logo atrás, nós também temos portado a distribuição Debian para essas
 arquiteturas. Em geral, este é um processo com um início lento (enquanto
 colocamos a libc e o ligador dinâmico para funcionar tranquilamente), para
 então passarmos para um trabalho relativamente rotineiro, embora demorado,
 de tentar recompilar todos os nossos pacotes nas novas arquiteturas.
</p>
<p>
 O Debian é um sistema operacional (SO), não um kernel (na verdade, é mais do
 que um SO, pois inclui milhares de programas aplicativos). Nesse sentido,
 enquanto a maioria dos portes Debian são baseados no Linux, também existem
 portes baseados nos kernels FreeBSD, NetBSD e Hurd.
</p>

<div class="important">
<p>
 Esta é uma página em construção. Nem todos os portes possuem
 páginas ainda, e a maioria deles está em sites externos. Estamos trabalhando para
 coletar informações sobre todos os portes, para então serem espelhadas
 juntamente com o site do Debian.
 Mais portes estão <a href="https://wiki.debian.org/CategoryPorts">listados</a>
 na wiki do Debian.
</p>
</div>

<toc-add-entry name="portlist-released">Lista de portes oficiais</toc-add-entry>

<p>
Esses portes são as arquiteturas oficialmente suportadas pelo projeto Debian, e
fazem parte de um lançamento oficial ou farão parte de um lançamento futuro.
</p>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Porte</th>
<th>Arquitetura</th>
<th>Descrição</th>
<th>Adicionado</th>
<th>Estado</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Porte para os processadores x86 de 64 bits, para suportar espaços de usuário
de 32 e 64 bits.
O porte suporta os processadores de 64 bits Opteron, Athlon e Sempron da AMD e
processadores da Intel com suporte a Intel 64, incluindo o Pentium D e várias
séries Xeon e Core.
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>ARM de 64 bits (AArch64)</td>
<td>Porte para a arquitetura ARM de 64 bits com o novo conjunto de instruções de
64 bits da versão 8 (chamado AArch64), para processos como Applied Micro X-Gene,
AMD Seattle e Cavium ThunderX.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Porte para a arquitetura ARM little-endian de 32 bits usando o Embedded ABI,
que suporta CPUs ARM compatíveis com o conjunto de instruções v5te. Este porte
não aproveita unidades de ponto flutuante (FPU).</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Porte para a arquitetura ARM little-endian de 32 bits para placas e
dispositivos fornecidos com uma unidade de ponto flutuante (FPU) e outros
recursos modernos de CPU ARM. Este porte requer pelo menos uma CPU ARMv7 com
suporte de ponto flutuante Thumb-2 e VFPv3-D16.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Porte para processadores x86 de 32 bits, onde o Linux foi
originalmente desenvolvido para processadores Intel 386, por isso o nome
abreviado. O Debian oferece suporte a todos os processadores IA-32, fabricados
pela Intel (incluindo toda a série Pentium e as máquinas Core Duo recentes em
modo 32-bit), AMD (K6 e todas as séries Athlon e séries Athlon64 em modo
32-bit), Cyrix e outros fabricantes.</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (modo little-endian de 64 bits)</td>
<td>
Porte para o ABI N64 little-endian para o MIPS64r1 ISA e hardware de ponto
flutuante.
</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Porte para a arquitetura POWER little-endian de 64 bits, que utiliza a
nova Open Power ELFv2 ABI.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-bit little endian)</td>
<td>Porte para o <a href="https://riscv.org/">RISC-V</a> little-endian de 64 bits,
um ISA gratuito/aberto.</td>
<td>13</td>
<td>testing</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Porte para mainframes IBM System z com espaço de usuário de 64 bits.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">lançado</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">Lista de outros portes</toc-add-entry>

<p>
Esses portes são esforços de trabalho em andamento que pretendem, eventualmente,
serem promovidos para arquiteturas lançadas oficialmente; portes que já foram
oficialmente suportados mas pararam de ser lançados porque falharam na
qualificação de lançamento ou tiveram interesse limitado do(a) desenvolvedor(a);
ou portes que não são mais desenvolvidos e estão listados por interesse histórico.
</p>

<p>
Esses portes, quando ainda são mantidos ativamente, estão disponíveis na
infraestrutura <url "https://www.ports.debian.org/">.
</p>

<div class="tip">
<p>
 Existem imagens de instalação não oficiais disponíveis para alguns dos
 seguintes portes no
 <url "https://cdimage.debian.org/cdimage/ports"/>.
 Essas imagens são mantidas pelos times dos portes correspondentes do Debian.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Porte</th>
<th>Arquitetura</th>
<th>Descrição</th>
<th>Adicionado</th>
<th>Descartado</th>
<th>Estado</th>
<th>Substituído por</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Porte para a arquitetura RISC Alpha 64 bits.</td>
<td>2.1</td>
<td>6.0</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Porte para a arquitetura ARM usando um velho ABI.
</td>
<td>2.2</td>
<td>6.0</td>
<td>encerrado</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel RISC de 32 bits</td>
<td>Porte para a arquitetura Atmel RISC de 32 bits, AVR32. </td>
<td>-</td>
<td>-</td>
<td>encerrado</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Porte para arquitetura PA-RISC da Hewlett-Packard.
</td>
<td>3.0</td>
<td>6.0</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Porte para o sistema operacional GNU Hurd, para processadores x86 de 32 bits.
</td>
<td>-</td>
<td>-</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Porte para o sistema operacional GNU Hurd, para processadores x86 de 64 bits.
Suporta apenas 64 bits, não 32 bits junto com 64 bits.
</td>
<td>-</td>
<td>-</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Porte da primeira arquitetura de 64 bits da Intel. Nota: este porte não
deve ser confundido com as últimas extensões de 64 bits da Intel para os
processadores Pentium 4 e Celeron, chamadas Intel 64; para estes portes veja o
porte amd64.
</td>
<td>3.0</td>
<td>8</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Porte para o kernel FreeBSD usando a glibc.
Foi lançado como a primeira versão não Linux do Debian como uma prévia da tecnologia.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">encerrado</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Porte para o kernel FreeBSD usando a glibc.
Foi lançado como a primeira versão não Linux do Debian como uma prévia da tecnologia.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">encerrado</a></td>

<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/LoongArch">loong64</a></td>
<td>LoongArch (64-bit little endian)</td>
<td>Porte para a arquitetura LoongArch de 64 bits little-endian.</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Porte para o microprocessador RISC de 32 bits da Renesas Technology.</td>
<td>-</td>
<td>-</td>
<td>encerrado</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Porte da série de processadores Motorola m68k — em particular,
as estações de trabalho Sun3, os computadores pessoais Apple Macintosh e os
computadores pessoais Atari e Amiga.</td>
<td>2.0</td>
<td>4.0</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (modo big-endian)</td>
<td>Porte para a arquitetura MIPS, que é usada em máquinas SGI (debian-mips —
big-endian).
</td>
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">encerrado</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (modo little-endian)</td>
<td>Porte para a arquitetura MIPS, que é usada em estações DEC Digitais
(debian-mipsel — little-endian).
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">encerrado</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Porte para o kernel NetBSD e libc, para processadores x86 de 32 bits.
</td>
<td>-</td>
<td>-</td>
<td>encerrado</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>Porte do kernel NetBSD e lib, para processadores Alpha de 64 bits.
</td>
<td>-</td>
<td>-</td>
<td>encerrado</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Porte para a CPU de código aberto <a href="https://openrisc.io/">OpenRISC</a> 1200.</td>
<td>-</td>
<td>-</td>
<td>encerrado</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Para para vários modelos Apple Macintosh PowerMac e máquinas de arquitetura
aberta CHRP e PReP.</td>
<td>2.2</td>
<td>9</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Porte para o hardware "Signal Processing Engine" presente em processadores de
32 bits "e500" de baixo consumo da Freescale e IMB.
</td>
<td>-</td>
<td>-</td>
<td>encerrado</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 e zSeries</td>
<td>Porte para os servidores S/390 da IBM.
</td>
<td>3.0</td>
<td>8</td>
<td>encerrado</td>
<td>s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Porte para máquinas de trabalho Sun UltraSPARC, bem como alguns de seus
sucessores nas arquiteturas sun4.
</td>
<td>2.1</td>
<td>8</td>
<td>encerrado</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>SPARC de 64 bits</td>
<td>
Porte para processadores SPARC de 64 bits.
</td>
<td>-</td>
<td>-</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Porte para processadores Hitachi SuperH. Também suporta o processador de
código aberto <a href="https://j-core.org/">J-Core</a>.
</td>
<td>-</td>
<td>-</td>
<td>portado</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>PC de 64 bits com ponteiros de 32 bits</td>
<td>
Porte para o amd64/x86_64 x32 ABI, que usa o conjunto de instruções amd64, mas
com ponteiros de 32 bits, para combinar o maior conjunto de registros desse ISA
com a menor memória e espaço de cache resultante de ponteiros de 32 bits.
</td>
<td>-</td>
<td>-</td>
<td>portado</td>
<td>-</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Muitos dos nomes de computadores e processadores mencionados acima
são marcas comerciais e marcas registradas de seus respectivos fabricantes.
</p>
</div>
