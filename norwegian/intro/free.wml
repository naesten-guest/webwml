#use wml::debian::template title="Hva betyr fri?" MAINPAGE="true"
#use wml::debian::translation-check translation="8d8a7b1eda812274f83d0370486ddb709d4f7d54" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Tor Slettnes (tor@slett.net)
# Oppdatert av Hans F. Nordhaug <hansfn@gmail.com>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Fri som i ...?</a></li>
    <li><a href="#licenses">Programvarelisenser</a></li>
    <li><a href="#choose">Hvordan velge en lisens?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> I februar 1998
forsøkte en gruppe å erstatte uttrykket 
<a href="https://www.gnu.org/philosophy/free-sw">fri programvare</a> med 
<a href="https://opensource.org/osd">åpen kildekode-programvare</a>.
Denne terminologidebatten reflekterer underliggende filosofiske forskjeller,
men de praktiske kravene til programvarelisenser og diskusjonen på resten
av denne siden, er essensielt den samme for fri programvare og åpen programvare.</p>
</aside>

<h2><a id="freesoftware">Fri som i ...?</a></h2>

<p>
    Mange som er ukjent med fri programvare blir forvirret fordi ordet
    "fri" og uttrykket "fri programvare" ikke brukes på den måten de
    venter seg.  For dem betyr fri "gratis".  En engelsk ordbok lister
    nesten 20 forskjellige meninger for "fri".  Bare en av disse er
    "gratis".  Alle de andre viser til frihet og mangel på tvang.  Når
    vi snakker om <em>fri programvare</em>, mener vi frihet, ikke
    pris.
</p>

  <p>
    Programvare som er fri bare i den meningen at du ikke trenger å
    betale for den er nesten ikke fri i det hele tatt.  Kanskje er du
    forhindret fra å videreformidle den, og du er nesten helt sikkert
    forhindret fra å forbedre den. Programvare som er lisensiert uten kostnader
    er vanligvis et våpen i en markedsføringskampanje for å fremme et
    beslektet produkt, eller å drive en mindre konkurrent ut av drift.
    Det fins ingen garanti for at den vil forbli gratis.
  </p>

  <p>
    For en uinnvidd er programvare enten fri eller ikke fri.
    Virkeligheten er en del mer komplisert enn som så.  For å forstå 
    hva folk mener når de kaller programvaren sin fri, må vi ta en 
    liten omvei inn i programvare-lisensenes verden.
  </p>

<h2><a id="licenses">Programvarelisenser</a></h2>

  <p>
    Opphavsrett (copyright) er en måte å beskytte rettighetene
    til forfatteren av særskilte typer arbeid.  I de fleste land er
    programvare du skriver automatisk beskyttet med kopirettigheter.
    En lisens er forfatterens måte å tillate bruk av
    sitt verk (programvare i dette tilfellet) for andre,
    på måter som er akseptabel for vedkommende.  Det er opp til
    forfatteren å inkludere en lisens som kunngjør på hvilke måter
    programvaren kan bli brukt.
  </p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="https://www.copyright.gov/">Les mer om opphavsrett</a></button></p>

<p>Naturligvis krever forskjellige omstendigheter forskjellige lisenser.
Programvarebedrifter leter etter måter å beskytte sine verdier på og utgir
derfor ofte kun kompilert kode som ikke kan leses av mennesker. De legger 
også mange restriksjoner på bruken av programvaren. På den andre siden fokuserer 
forfattere av fri programvare primært på forskjellige regler, ofte en
kombinasjon av følgende punkter:</p>

  <ul>
    <li>
      Å ikke la koden sin bli brukt i komersiell programvare. Siden de
      utgir koden sin for bruk allmen bruk, vil de se til at ikke
      andre stjeler den.  I dette tilfellet ses kildekoden på som en
      forvaltning: Du kan bruke den, såfremt du spiller med de samme
      reglene.
    </li>

    <li>
      Beskytte renheten av kodens forfatterskap.  Folk er stolte av
      arbeidet sitt og vil ikke at andre skal komme og fjerne navnet
      sitt eller påstå at de skrev det.
    </li>

    <li>
      Distribusjon av kildekode. Et av problemene med det meste av
      proprietær kode er at du ikke kan reparere feil eller tilpasse
      den, siden kildekoden er ikke tilgjengelig.  Det kan også hende
      at bedriften som skrev koden ikke lenger støtter maskinvaren
      din.  Mange frie lisenser tvinger distribusjon av kildekoden.
      Dette beskytter brukeren ved å gi dem adgang til å tilpasse
      programvaren for sine behov.
    </li>

    <li>
      Tvinge alt arbeid som inkluderer deres arbeid (slike arbeider
      kalles <em>nedstammet arbeid</em>, eller <em>deriverte
      verker</em>) til å bruke den samme lisensen.
    </li>
  </ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Tre av de mest utbredte fri 
programvare-lisensene er 
<a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a>, 
<a href="https://opensource.org/blog/license/artistic-2-0">Artistic License</a> og 
<a href="https://opensource.org/blog/license/bsd-3-clause">BSD Style License</a>.
</aside>


<h2><a id="choose">Hvordan velge en lisens?</a></h2>

<p>Noen ganger skriver folk sine egne lisenser, som kan være problematisk, 
og ikke blir sett på med blide øyne i fri programvare-fellesskapet. 
Alt for ofte er de brukte formuleringene enten tvetydig, eller folk lager 
betingelser som er i konflikt med hverandre. Å skrive en lisens som ville
holdt opp i retten er enda vanskeligere. Heldigvis er det allerede skrevet
en rekke lisenser som man kan velge mellom. De har følgende likhetstrekk:
</p>

<ul>
  <li>Brukeren kan installere programvaren på så mange maskiner han vil.</li>
  <li>Et ubegrenset antall personer kan bruke programvaren samtidig.</li>
  <li>Brukeren kan lage så mange kopier av programvaren han vil, og gi dem
    til hvem han vil (fri eller åpen videreformidling).</li>
  <li> Det er ingen restriksjoner mot å endre programvaren (bortsett
    fra å holde visse notiser vedlike).</li>
  <li> Det er ingen restriksjoner mot å distribuere, 
    eller til og med å selge, programvaren.</li>
</ul>

<p>
Det siste punktet, som tillater programvaren å bli solgt for fortjeneste,
ser ut til å stride mot hele vitsen med fri programvare.  Det er faktisk
en av styrkene. Siden lisensen tillater fri videreformidling, så snart 
en person får en kopi kan de gi den bort selv. De kan til og med prøve
å selge den. </p>

<p>
Selv om fri programvare ikke er helt fri for begrensninger (det er bare
å utgi noe til allmenn eie gjør det), gir det brukeren fleksibilitet til
å gjøre det han trenger for å få gjort arbeid. Samtidig beskytter det
rettighetene til forfatteren. Dette er skikkelig frihet.
Debian-projektet og dets medlemmer går sterkt inn for fri programvare. Vi har 
utformet <a href="../social_contract#guidelines">Debians retningslinjer for 
fri programvare (DFSG)</a>, for å ha en fornuftig definisjon av hva fri programvare 
er etter vår mening. Kun programvare som lever opp til DFSG, er tillatt 
i vår primære Debian-distribusjon.</p>

# Local variables:
# mode: sgml
# sgml-indent-data:t
# sgml-doctype:"../.doctype"
# End:
