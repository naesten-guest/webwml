#use wml::debian::template title="Debian Trixie &ndash; Installationsanleitung" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741"

<if-stable-release release="bookworm">
<p>Dies ist eine <strong>Betaversion</strong> der Installationsanleitung für
   Debian 13 (Codename Trixie), die noch nicht veröffentlicht ist.
   Die hier vorgestellten Informationen können aufgrund von Änderungen am
   Installer veraltet und/oder inkorrekt sein. Sie sind vielleicht an der <a
   href="../bookworm/installmanual">Installationsanleitung für Debian 12
   (Codename Bookworm)</a> interessiert, welches die neueste veröffentlichte Version
   von Debian ist; oder an der <a href="https://d-i.debian.org/manual/">\
   Entwicklungsversion der Installationsanleitung</a>, die die aktuellste
   Version dieses Dokuments darstellt.
</p>
</if-stable-release>

<p>Installationsanleitungen (auch als Dateien zum Download) sind für
   alle unterstützten Architekturen verfügbar:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>Wenn Sie Ihren Browser richtig auf Ihre Sprache eingestellt haben, können
   Sie den obigen Link verwenden, um automatisch die richtige HTML-Version zu
   bekommen &ndash; siehe auch <a href="$(HOME)/intro/cn">\
   Inhalts-Aushandlung</a>. Andernfalls müssen Sie selber aus der folgenden
   Tabelle die richtige Architektur, Sprache und das Format aussuchen.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Sprachen</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
