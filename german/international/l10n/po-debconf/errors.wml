#use wml::debian::template title="Fehler in übersetzten Debconf-Schablonen"
#include "$(ENGLISHDIR)/international/l10n/po-debconf/menu.inc"
#use wml::debian::translation-check translation="b339a77580e06b206bedadd0ee4df7dd5cae5ef8"
# $Id$
# Translation: Helge Kreutzm <kreutzm@itp.uni-hannover.de> 2005-12-12

<p>
   Sie können eine <a href="errors-by-pkg">alphabetische Liste von Paketen</a> 
   mit Fehlern in den übersetzten Debconf-Schablonen bekommen, oder die gleiche Liste,
   sortiert nach <a href="errors-by-maint">Betreuern</a>.
</p>

<p>
   Um Platz zu sparen enthalten beide Listen Schlüsselwörter anstatt
   aussagekräftiger Meldungen; diese Schlüsselwörter sind hier erklärt. Einige der Fehler
   sollten von den Übersetzern, andere dagegen von den Betreuern behoben
   werden.
</p>

<h3>Fehler, die in der Verantwortung der Übersetzer liegen</h3>

<dl>
  <dt><a name="charsetname">invalid-charset-name-in-po</a></dt>
  <dd>
    PO-Dateien müssen einen gültigen Zeichensatz im Content-Type:-Feld des
    Dateikopfs haben. Der Zeichensatz unterliegt der Wahl des Übersetzers und die
    PO-Datei sollte nur vom Übersetzer selbst geändert werden, es sei denn, der 
    Betreuer ist sich absolut sicher, was er tut.
  </dd>
  <dt><a name="charset">wrong-charset</a></dt>
  <dd>
    Der im Content-Type:-Feld im Kopfteil der PO-Datei spezifizierte Zeichensatz ist
    nicht der gleiche wie der in der PO-Datei verwendete. Der Zeichensatz
    unterliegt der Wahl des Übersetzers und die PO-Datei sollte nur vom Übersetzer
    selbst geändert werden, es sei denn, der Betreuer ist sich absolut sicher, 
    was er tut. Diese Dateien sind aktuell überhaupt nicht zu verwenden und der Betreuer
    sollte daher die Übersetzer um korrigierte Dateien bitten.
  </dd>
  <dt><a name="invalidpo">invalid-po</a></dt>
  <dd>
    Die PO-Datei ist nicht gültig. Die Gründe können vielfältig sein. Die 
    Ausgabe von <q>msgfmt</q> sollte dem Übersetzer helfen, die Dateien
    zu korrigieren. Diese Dateien sind aktuell überhaupt nicht zu verwenden
    und der Betreuer sollte daher den Übersetzer um korrigierte Dateien bitten.
  </dd>
</dl>

<h3>Fehler, die in der Verantwortung des Betreuers liegen</h3>

<dl>
  <dt><a name="unknownlanguage">unknown-language</a></dt>
  <dd>
    Die PO-Datei hat einen unbekannten Sprach-Code. Der <q>basename</q> der
    Datei sollte ein gültiger Sprach-Code sein. Die Kopfzeilen sollten helfen
    herauszufinden, auf welche Sprache sie sich bezieht, andernfalls ist es
    zwecklos, sie in ein Paket aufzunehmen, da sie nicht verwendet wird.
  </dd>
  <dt><a name="missingfile">missing-file-in-POTFILES.in</a></dt>
  <dd>
    Die debian/po/POTFILES.in-Datei bezieht sich auf nicht-existierende
    Schablonen-Dateien. Dieser Fehler ist oft nach der Umbenennung oder
    Entfernung von Schablonen-Dateien zu finden. Es liegt in der Verantwortung
    des Betreuers, diese Datei zu korrigieren, die PO-Dateien zu aktualisieren oder
    um aktualisierte Übersetzungen zu bitten. Übersetzer sollten 
    <strong>nicht</strong> an diesen Paketen arbeiten, da die 
    templates.pot-Datei typischerweise stark veraltet ist.
  </dd>
  <dt><a name="template">not-up-to-date-templates.pot</a></dt>
  <dd>
    Die debian/po/templates.pot-Datei wurde nicht mit der Schablonen-Datei
    synchronisiert. Der Betreuer sollte das Paket durch Hinzufügen von
    debconf-updatepo im <q>clean</q>-Ziel von debian/rules korrigieren.
    Übersetzer sollten zuerst debconf-updatepo aufrufen, wenn sie mit dem
    Quell-Paket arbeiten. Die PO- und POT-Dateien auf der Webseite sollten
    aktuell sein.
  </dd>
  <dt><a name="po">not-up-to-date-po-file</a></dt>
  <dd>
    Die aufgelisteten Dateien wurden nicht mit den Schablonen synchronisiert.
    Der Betreuer sollte das Paket durch Hinzufügen von debconf-updatepo 
    im <q>clean</q>-Ziel von debian/rules korrigieren. Übersetzer sollten 
    zuerst debconf-updatepo aufrufen, wenn sie mit dem Quell-Paket arbeiten. Die
    PO-Dateien auf der Webseite sollten aktuell sein.
  </dd>
</dl>
