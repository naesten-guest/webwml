#use wml::debian::template title="Debian &ldquo;jessie&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian <current_release_jessie> was
released <a href="$(HOME)/News/<current_release_newsurl_jessie/>"><current_release_date_jessie></a>.
<ifneq "8.0" "<current_release>"
  "Debian 8.0 was initially released on <:=spokendate('2015-04-26'):>."
/>
The release included many major
changes, described in
our <a href="$(HOME)/News/2015/20150426">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian 8 has been superseded by
<a href="../stretch/">Debian 9 (<q>stretch</q>)</a>.
Regular security support updates have been discontinued as of <:=spokendate('2018-06-17'):>.
</strong></p>

<p><strong>Jessie also had benefited from Long Term Support (LTS) until
the end of June 2020. The LTS was limited to i386, amd64, armel and armhf.
For more information, please refer to the <a
href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
</strong></p>

<p>To obtain and install Debian, see
the installation information page and the
Installation Guide. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>Architectures supported during LTS support:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>

<p>When initially releasing Jessie, it was supported by these architectures:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
report other issues to us.</p>
