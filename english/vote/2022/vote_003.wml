<define-tag pagetitle>General Resolution: non-free firmware</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Discussion Period:</th>
	<td>2022-08-18</td>
	<td>2022-09-15</td>
      </tr>
      <tr>
	<th>Voting period:</th>
	<td>Sunday 2022-09-18 00:00:00 UTC</td>
	<td>Saturday 2022-10-01 23:59:59 UTC</td>
      </tr>
    </table>

    The discussion period has been extended with 7 days by the Debian Project Leader.
    [<a href='https://lists.debian.org/debian-vote/2022/09/msg00037.html'>mail</a>]</li>

    <vproposera />
    <p>Steve McIntyre [<email 93sam@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00001.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00194.html'>amendment</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00002.html'>mail</a>]</li>
        <li>Luca Boccassi [<email bluca@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00003.html'>mail</a>]</li>
        <li>Ansgar [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00004.html'>mail</a>]</li>
        <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00005.html'>mail</a>]</li>
        <li>Sebastian Ramacher [<email sramacher@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00006.html'>mail</a>]</li>
        <li>Samuel Henrique [<email samueloph@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00007.html'>mail</a>]</li>
        <li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00008.html'>mail</a>]</li>
        <li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00009.html'>mail</a>]</li>
        <li>Joerg Jaspert [<email joerg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00011.html'>mail</a>]</li>
        <li>Cyril Brulebois [<email kibi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00014.html'>mail</a>]</li>
        <li>Iain Lane [<email laney@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00015.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00023.html'>mail</a>]</li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00029.html'>mail</a>]</li>
        <li>Anton Gladky [<email gladk@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00030.html'>mail</a>]</li>
        <li>Moritz Mühlenhoff [<email jmm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00031.html'>mail</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00034.html'>mail</a>]</li>
    </ol>
    <vtexta />
<h3>Choice 1: Only one installer, including non-free firmware</h3>

<p>We will include non-free firmware packages from the
"non-free-firmware" section of the Debian archive on our official
media (installer images and live images). The included firmware
binaries will <b>normally</b> be enabled by default where the system
determines that they are required, but where possible we will include
ways for users to disable this at boot (boot menu option, kernel
command line etc.).</p>

<p>When the installer/live system is running we will provide information
to the user about what firmware has been loaded (both free and
non-free), and we will also store that information on the target
system such that users will be able to find it later.
Where non-free firmware is found to be necessary, the target system
will <b>also</b> be configured to use the non-free-firmware component by
default in the apt sources.list file.
Our users should
receive security updates and important fixes to firmware binaries just
like any other installed software.</p>

<p>We will publish these images as official Debian media, replacing the
current media sets that do not include non-free firmware packages.</p>

    <vproposerb />
    <p>Gunnar Wolf [<email gwolf@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00046.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00215.html'>amendment</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00047.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00048.html'>mail</a>]</li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00053.html'>mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00055.html'>mail</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00060.html'>mail</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00093.html'>mail</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00120.html'>mail</a>]</li>
        <li>Jonathan Carter [<email jcc@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00122.html'>mail</a>]</li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00168.html'>mail</a>]</li>
    </ol>
    <vtextb />
<h3>Choice 2: Recommend installer containing non-free firmware</h3>

<p>We will include non-free firmware packages from the
"non-free-firmware" section of the Debian archive on our official
media (installer images and live images). The included firmware
binaries will <b>normally</b> be enabled by default where the system
determines that they are required, but where possible we will include
ways for users to disable this at boot (boot menu option, kernel
command line etc.).</p>

<p>When the installer/live system is running we will provide information
to the user about what firmware has been loaded (both free and
non-free), and we will also store that information on the target
system such that users will be able to find it later.
Where non-free firmware is found to be necessary, the target system
will <b>also</b> be configured to use the non-free-firmware component by
default in the apt sources.list file.
Our users should
receive security updates and important fixes to firmware binaries just
like any other installed software.</p>

<p>While we will publish these images as official Debian media, they will
<b>not</b> replace the current media sets that do not include non-free
firmware packages, but offered alongside. Images that do include
non-free firmware will be presented more prominently, so that
newcomers will find them more easily; fully-free images will not be
hidden away; they will be linked from the same project pages, but with
less visual priority.</p>

    <vproposerc />
    <p>Bart Martens [<email bartm@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00106.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/09/msg00041.html'>amendment</a>]
    </p>
    <vsecondsc />
    <ol>
        <li>Stefano Zacchiroli [<email zack@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00116.html'>mail</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00119.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00123.html'>mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00128.html'>mail</a>]</li>
        <li>Philip Rinn [<email rinni@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00160.html'>mail</a>]</li>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00044.html'>mail</a>]</li>
        <li>Paul Wise [<email pabs@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00055.html'>mail</a>]</li>
        <li>Simon Josefsson [<email jas@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00086.html'>mail</a>]</li>
    </ol>
    <vtextc />
<h3>Choice 3: Allow presenting non-free installers alongside the free one</h3>

<p>The Debian project is permitted to make distribution media (installer images
and live images) containing non-free software from the Debian archive available
for download alongside with the free media in a way that the user is informed
before downloading which media are the free ones.
</p>

    <vproposerd />
    <p>Simon Josefsson [<email jas@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00173.html'>text of proposal</a>]
    </p>
    <vsecondsd />
    <ol>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00068.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00071.html'>mail</a>]</li>
        <li>Hubert Chathi [<email uhoreg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00074.html'>mail</a>]</li>
        <li>Guilhem Moulin [<email guilhem@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00167.html'>mail</a>]</li>
        <li>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00211.html'>mail</a>]</li>
        <li>Shengjing Zhu [<email zhsj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00000.html'>mail</a>]</li>
    </ol>
    <vtextd />
<h3>Choice 4: Installer with non-free software is not part of Debian</h3>

<p>We continue to stand by the spirit of the Debian Social Contract §1
which says:</p>

<pre>
   Debian will remain 100% free

   We provide the guidelines that we use to determine if a work is
   "free" in the document entitled "The Debian Free Software
   Guidelines". We promise that the Debian system and all its components
   will be free according to these guidelines. We will support people
   who create or use both free and non-free works on Debian. We will
   never make the system require the use of a non-free component.
</pre>

<p>Therefore we will not include any non-free software in Debian, nor in the
main archive or installer/live/cloud or other official images, and will
not enable anything from non-free or contrib by default.</p>

<p>We also continue to stand by the spirit of the Debian Social Contract §5
which says:</p>

<pre>
   Works that do not meet our free software standards

   We acknowledge that some of our users require the use of works that
   do not conform to the Debian Free Software Guidelines. We have
   created "contrib" and "non-free" areas in our archive for these
   works. The packages in these areas are not part of the Debian system,
   although they have been configured for use with Debian. We encourage
   CD manufacturers to read the licenses of the packages in these areas
   and determine if they can distribute the packages on their CDs. Thus,
   although non-free works are not a part of Debian, we support their
   use and provide infrastructure for non-free packages (such as our bug
   tracking system and mailing lists).
</pre>

<p>Thereby reinforcing the interpretation that any installer or image with
non-free software on it is not part of the Debian system, but that we
support their use and welcome others to distribute such work.
</p>

    <vproposere />
    <p>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00038.html'>text of proposal</a>]
    </p>
    <vsecondse />
    <ol>
        <li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00039.html'>mail</a>]</li>
        <li>Ansgar [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00040.html'>mail</a>]</li>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00045.html'>mail</a>]</li>
        <li>Kunal Mehta [<email legoktm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00046.html'>mail</a>]</li>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00047.html'>mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00053.html'>mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00071.html'>mail</a>]</li>
    </ol>
    <vtexte />
<h3>Choice 5: Change SC for non-free firmware in installer, one installer</h3>
<p>This ballot option supersedes the Debian Social Contract (a foundation document) under point 4.1.5 of the constitution and thus requires a 3:1 majority.</p>

<p>The Debian Social Contract is replaced with a new version that is
identical to the current version in all respects except that it adds the
following sentence to the end of point 5:</p>

<pre>
    The Debian official media may include firmware that is otherwise not
    part of the Debian system to enable use of Debian with hardware that
    requires such firmware.
</pre>

<p>The Debian Project also makes the following statement on an issue of the day:</p>

<p>We will include non-free firmware packages from the "non-free-firmware"
section of the Debian archive on our official media (installer images and
live images). The included firmware binaries will normally be enabled by
default where the system determines that they are required, but where
possible we will include ways for users to disable this at boot (boot menu
option, kernel command line etc.).</p>

<p>When the installer/live system is running we will provide information to
the user about what firmware has been loaded (both free and non-free), and
we will also store that information on the target system such that users
will be able to find it later. Where non-free firmware is found to be
necessary, the target system will also be configured to use the
non-free-firmware component by default in the apt sources.list file. Our
users should receive security updates and important fixes to firmware
binaries just like any other installed software.</p>

<p>We will publish these images as official Debian media, replacing the
current media sets that do not include non-free firmware packages.</p>

    <vproposerf />
    <p>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00166.html'>text of proposal</a>]
    </p>
    <vsecondsf />
    <ol>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00167.html'>mail</a>]</li>
        <li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00168.html'>mail</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00169.html'>mail</a>]</li>
        <li>Étienne Mollier [<email emollier@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00170.html'>mail</a>]</li>
        <li>Judit Foglszinger [<email urbec@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00171.html'>mail</a>]</li>
        <li>David Prévot [<email taffit@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00173.html'>mail</a>]</li>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00174.html'>mail</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00176.html'>mail</a>]</li>
        <li>Didier Raboud [<email odyx@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00180.html'>mail</a>]</li>
    </ol>
    <vtextf />
<h3>Choice 6: Change SC for non-free firmware in installer, keep both installers</h3>
<p>This ballot option supersedes the Debian Social Contract (a foundation document) under point 4.1.5 of the constitution and thus requires a 3:1 majority.</p>

<p>The Debian Social Contract is replaced with a new version that is identical to the current version in all respects except that it adds the following sentence to the end of point 5:</p>

<pre>
    The Debian official media may include firmware that is otherwise not
    part of the Debian system to enable use of Debian with hardware that
    requires such firmware.
</pre>

<p>The Debian Project also makes the following statement on an issue of the day:</p>

<p>We will include non-free firmware packages from the "non-free-firmware" section of the Debian archive on our official media (installer images and live images).
The included firmware binaries will normally be enabled by default where the system determines that they are required, but where possible we will include ways for users to disable this at boot (boot menu option, kernel command line etc.).</p>
                                                                                                                                                                                              
<p>When the installer/live system is running we will provide information to the user about what firmware has been loaded (both free and non-free), and we will also store that information on the target system such that users will be able to find it later. Where non-free firmware is found to be necessary, the target system will also be configured to use the non-free-firmware component by default in the apt sources.list file. Our users should receive security updates and important fixes to firmware binaries just like any other installed software.</p>
                                                                                                                                                                                              
<p>We will publish these images as official Debian media, alongside the current media sets that do not include non-free firmware packages.</p>

    <vquorum />

     <p>
        With the current list of <a href="vote_003_quorum.log">voting
          developers</a>, we have:
     </p>
    <pre>
#include 'vote_003_quorum.txt'
    </pre>
#include 'vote_003_quorum.src'


    <vstatistics />
    <p>
	For this GR, like always,
#                <a href="https://vote.debian.org/~secretary/gr_non_free_firmware/">statistics</a>
               <a href="suppl_003_stats">statistics</a>
             will be gathered about ballots received and
             acknowledgements sent periodically during the voting
             period.
               Additionally, the list of <a
             href="vote_003_voters.txt">voters</a> will be
             recorded. Also, the <a href="vote_003_tally.txt">tally
             sheet</a> will also be made available to be viewed.
         </p>

    <vmajorityreq />
    <p>
      Proposal 5 and 6 need a 3:1 super majority
    </p>
#include 'vote_003_majority.src'

    <voutcome />
#include 'vote_003_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>

