#use wml::debian::template title="Platform for Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>DPL Platform</SMALL><BR>
    <SMALL>2021-03-15</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>

<H2 CLASS="section">1. Personal Introduction</H2>

<P>Hi, my name is Jonathan Carter, also known as <EM>highvoltage</EM>, with the Debian account name of <EM>jcc</EM>, and I'm running for a second DPL term. </P>

<P>I am a 39 year old Debian Developer who have various areas of interest within the Debian project. Like many other individuals and organisations
that contribute to Debian, I contribute to the project because I enjoy using the products of our work and would like that to continue to exist,
Debian remains unique in delivering a distribution unencumbered by stringent licensing limitations, while delivering stable releases and security
updates along with a very large selection of software packages. I believe that Debian is an extremely important software project and it's social
contract adds to the project's uniqueness by explicitly putting the priorities of our users first. We do not answer to shareholders or this quarter's
bottom line, but instead we work together to try to find the best solutions for our users. While this isn't always easy or straightforward, I find
it very rewarding and work that is worth while doing.</P>


<H2 CLASS="section">2. Why I'm running for DPL</H2>

<P>
During my last term, I aimed to bring a sense of stability and normality to the project. Despite all the chaos that has prevailed in
the world around us, and with the help of other project members, I think that we've succeeded in establishing that stability.
Overall, our project has many strong points, we have an active and vibrant community, we have enough funds to carry the project for a while,
we're also on track for making a new stable release this year with the initial freezes for Debian 11 happening on schedule.
</P>

<P>
I would really like to see a growth spurt for Debian, but I don't think that the project is ready for that. We've grown a lot over the years
already, but haven't kept up with that growth administratively. We now have several weak spots that I believe should be addressed before
the project grows significantly further or it may have unwanted consequences in the future.
</P>

<P>
In this term, I'd like to continue the work that I started in my previous term, with an additional focus on improving our administrative
problems.
</P>


<H2 CLASS="section">3. Approach</H2>

<H3 CLASS="subsection"> 3.1. Addressing administrative weak spots </H3>

<P><B>Improve accounting.</B> It's really hard to keep track of how much money we have and how much we spend (and exactly what we spend it on),
we don't have much transparency on our finances, not even to our project members. I've posted some financial updates to debian-private
and included some of it in my "bits from the DPL" talks, but I believe it's really something that our treasurer team should have oversight
of. Typically those reports are based on public records from our TOs, which may be up to several months old. The treasurer team has some
great ideas to address some of this, like line-item accounting with shared item codes between TOs so that we could get better real-time
reporting. Overall, I would like for the treasurer team to be better enabled to do the work they set out to do, and ideally reduce some
load from the DPL, I'll address some of that in the following points since some of the issues are intermingled.</P>

<P><B>Implement an expendature policy.</B> In order to do our work in Debian, we often have to spend some of the project's money.
Sometimes this is for meetings (travel, refreshments, venue, etc), for hardware (specialised equipment, personal hardware, hosting costs, etc) or for
administrative purposes (like domain names, trademark fees, legal fees, etc). I would like to implement a policy that makes it easier
for Debian Developers to understand what they could spend money on and how. During my term I have learned that there is a lot of inconsistency
among project members on what they believe Debian money could be used for, often causing members not to use Debian money which could
have been used to the benefit of the project. I believe that a proper policy could encourage better spending and foster project growth.
I also think that if we have a proper expendature policy, we could delegate spending approvals. For example, we could have a checklist
that if met, the delegation can approve the expendature without DPL intervention, reducing some load on the DPL. I believe that it would
be a natural fit for such a task to be performed by the treasurer team, although if they feel it's a bad fit then it could also be a new
delegation.</P>

<P><B>Trusted organisations.</B> Our relationships with TOs need more work. Last year, we had a (at least minor) dispute with SPI regarding
administrative fees on DebConf donations. It turns out that there were at least two different (conflicting) agreements made between different
sets of people who had discussions on the matter. In the past, we may have been small enough so that verbal "gentleman agreements" might have
worked, but we've clearly grown to a point where we really need to commit any agreements to paper. The situation with the DebConf donations
caused enough grief already, but unfortunately it gets a lot worse. In a legal matter, the members involved wanted to represent Debian, but
since Debian isn't incorporated as a registered organisation anywhere we felt it would be natural to fall back to SPI as our registered
organisation in that matter. The legal firm wanted the agreement/contract between Debian and SPI in order to continue, but alarmingly neither
myself nor SPI could find any such document and it doesn't seem to exist. We have critera for TOs on our wiki, but those do not equate to being
a legal agreement between two parties. We also have had two TOs that have gone defunct, and we have no idea what happened to the funds that was
held by those TOs. We've exhaustively tried to get hold of the individuals responsible and tried to get answers. Unfortunately, I fear that lack
of any kind of formal agreements might mean that we've lost some money permanently. I have lots of confidence in our current TOs, those being
SPI, Debian France and Debian.ch, but I do think that we need to better formalize our relationships with our trusted organisations and also
better enforce our requirements of TOs as listed on our <a href="https://wiki.debian.org/Teams/DPL/TrustedOrganizationCriteria">wiki</a>.
</P>

<P><B>Consider formal registration of the Debian organisation</B>. I'd like to initiate discussions on registering Debian as a formal organisation.
Last year, Brian Gupta's DPL campaign largely revolved around founding a Foundation for the Debian project. I agree with many of his reasons for
wanting to pursue that. This is probably a good time to clarify that my DPL campaign is neither a referendum on this matter and neither is this
a campaign promise, I believe it's something that we need to seriously address together as a project. Above I have mentioned that I believe we
need better agreements between our TOs. Some people with legal experience (although not lawyers) have pointed out to me that an agreement between
an unregistered association of volunteers and a TO won't mean much in many countries, and that we would need to make such an agreement from our
registered organisation for it to mean anything. From what I've read so far, a Foundation would be overkill for Debian's purposes. It would need
a board and would have stringent auditing requirements. From what I understand, a simple registered non-profit organisation where its membership
equals Debian project membership may be a lot more appriate. From what I understand, such a registered organization would also decrease individual
legal liability within the project. I believe that we should spend some money on consulting to find out what might be the best for the project and
collectively decide on how we want to proceed, it will likely result in a general resolution ballot.</P>


<H3 CLASS="subsection"> 3.2. Continuation of goals for current term </H3>


<P><B>Local Teams.</B> I'm very happy that the local teams effort got rebooted at DebConf 20. I'd like to pour some more energy into this so
that we're in a good position to have a lot more in-person events when it's safe to do so again, and that local teams have everything they need
to attract curious people to their events. I'd like the local teams administration to grow in to a delegation that could take on some of the
responsibilities of the DPL when it comes to local team decisions. I still believe that local teams organising meetings and events is a good
strategy to increase both diversity and participation within the project and that it should be a priority.</P>

<P><B>Continue to improve onboarding.</B> I'd like to specifically thank Enrico Zini for his work on making the new maintainers process easier
over the last year, that has certainly already made major improvements and has very likely resulted in more DDs and DMs than we would've had
without those improvements. I do think we can do even better and have some introductory videos and documents on the tools we use and how they
work (like Salsa, the wiki, mailing lists, etc). I'd like some further project input on whether we should consider competitions or bounties
for such content.</P>

<H2> 4. Acknowledgements </H2>

<UL>
<LI>I used <a href="https://www.debian.org/vote/2010/platforms/zack">Zack's platform layout</a> as a base for this one.</LI>
</UL>

<!--
<H2> A. Rebuttals </H2>

<H3> A.1. Sruthi Chandran </H3>

<P>
</P>
-->

<H2> A. Changelog </H2>

<P> This platform is version controlled in a <a href="https://salsa.debian.org/jcc/dpl-platform">git repository.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/3.0.0">3.0.0</a>: New platform for 2021 DPL elections.</LI>
</UL>

<BR>

</DIV> <!-- END MAIN -->


