#use wml::debian::template title="debian.community domain"
#use wml::debian::faqs

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p style="display: block; border: solid; padding: 1em; background-color: #FFF29F;">
 <i class="fa fa-unlink fa-3x"></i>
 If you reached this page via a link to <a
 href="https://debian.community">debian.community</a>, the page you requested
 no longer exists. Read on to find out why.
</p>

<ul class="toc">
 <li><a href="#background">Background</a></li>
 <li><a href="#statements">Previous Statements</a></li>
 <li><a href="#faq">Frequently Asked Questions</a></li>
</ul>

<h2><a id="background">Background</a></h2>

<p>
 In July 2022 the World Intellectual Property Organization determined that
 the domain <a href="https://debian.community/">debian.community</a> had been
 registered in bad faith and was being used to tarnish the trademarks of the
 Debian Project. It ordered that the domain be handed to the Project.
</p>

<h2><a id="statements">Previous statements</a></h2>

<h3><a id="statement-debian">
 Debian Project: harassment from Daniel Pocock
</a></h3>

<p>
 <i>First published: <a href="$(HOME)/News/2021/20211117">November
17th, 2021</a></i>
</p>

<p>
Debian is aware of a number of public posts made about Debian and its
community members on a series of websites by a Mr Daniel Pocock, who
purports to be a Debian Developer.
</p>

<p>
Mr Pocock is not associated with Debian. He is neither a Debian Developer,
nor a member of the Debian community. He was formerly a Debian Developer,
but was expelled from the project some years ago for engaging in behaviour
which was destructive to Debian's reputation and to the community itself.
He has not been a member of the Debian Project since 2018. He is also
banned from participating in the Debian community in any form, including
through technical contributions, participating in online spaces, or
attending conferences and/or events. He has no right or standing to
represent Debian in any capacity, or to represent himself as a Debian
Developer or member of the Debian community.
</p>

<p>
In the time since he was expelled from the project, Mr Pocock has engaged
in an ongoing and extensive campaign of retaliatory harassment by making a
number of inflammatory and defamatory posts online, in particular on a
website which purports to be a Debian website. The contents of these posts
involve not only Debian, but also a number of its Developers and
volunteers. He has also continued to misrepresent himself as being a member
of the Debian Community in much of his communication and public
presentations. Please see this article for a list of the official Debian
communication channels. Legal action is being considered for, amongst other
things, defamation, malicious falsehood and harassment.
</p>

<p>
Debian stands together as a community, and against harassment. We have a
code of conduct that guides our response to harmful behaviour in our
community, and we will continue to act to protect our community and
volunteers. Please do not hesitate to contact the Debian Community team if
you have concerns or need support. In the meantime, all of Debian's and its
volunteers' rights are reserved.
</p>

<h3><a id="statement-other">Statements from other projects</a></h3>

<ul>
 <li>
  <a href="https://fsfe.org/about/legal/minutes/minutes-2019-10-12.en.pdf#page=17">
   Free Software Foundation Europe e.V.</a> (first published October 12, 2019)
 </li>
 <li>
  <a href="https://openlabs.cc/en/statement-we-have-been-a-target-of-disinformation-efforts-our-initial-reaction/">
   Open Labs</a> (first published May 26, 2021)
 </li>
 <li>
  <a href="https://communityblog.fedoraproject.org/statement-on-we-make-fedora/">
   Fedora</a> (first published January 31, 2022)
 </li>
</ul>

<h2><a id="faq">Frequently asked questions</a></h2>

<question>
 Why was the domain transferred to Debian?
</question>

<answer><p>
 Debian pursued a complaint with WIPO that the domain was being used for bad
 faith infringement of Debian's trademarks. In July 2022 the WIPO panel
 agreed that the previous registrant had no rights or interest in the
 trademarks and was using them in bad faith, and transferred the domain to
 the Debian Project.
</p></answer>

<question>
 What was Debian's objection to the content?
</question>
 
<answer><p>
 The content of the <a href="https://debian.community/">debian.community</a>
 site tarnished Debian's trademarks by associating them with unsubstantiated
 claims and links to cults, insinuations of enslavement and abuse of
 volunteers.
</p></answer>

<question>
 Who was behind the previous site?
</question>

<answer><p>
 The previous registrant of the domain was Free Software Contributors
 Association, an unregistered association in Switzerland. In its complaint
 to WIPO, Debian asserted that the association is an alter ego for 
 Daniel Pocock.
</p></answer>

<question>
 Can I read the WIPO judgement in full?
</question>

<answer><p>
 Yes, it is
 <a href="https://www.wipo.int/amc/en/domains/decisions/pdf/2022/d2022-1524.pdf">
 publicly archived</a>.
</p></answer>

<question>
 Did the panel find that the articles were defamatory?
</question>

<answer><p>
 The panel was not permitted to make a finding either way - it is beyond the
 panel's jurisdiction. The panel was only in a position to find that the
 registrant registered and used the domain in bad faith to tarnish Debian's
 trademarks.
</p></answer>

<question>
 Will Debian be taking any other action?
</question>

<answer><p>
 This information cannot be publicly disclosed. The Debian Project continues
 to monitor the situation and consult with its legal counsel.
</p></answer>

<h2><a id="further-info">Further information<a/></h2>

<ul>
 <li>
  <a href="$(HOME)/intro/people">
   Debian Project: People: Who we are and what we do</a>
 </li>
 <li>
  <a href="$(HOME)/intro/philosophy">
   Debian Project: Our Philosophy: Why we do it and how we do it</a>
 </li>
</ul>

