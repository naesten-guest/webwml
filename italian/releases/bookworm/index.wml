#use wml::debian::template title="Informazioni sul rilascio di Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4a4923573595a8539cd9aca5189940d45bcbbb29" maintainer="Luca Monducci"


<p>Debian GNU/Linux <current_release_bookworm> è stata rilasciata il
<a href="$(HOME)/News/<current_release_newsurl_bookworm/>">
<current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
	"Il rilascio iniziale di Debian 12.0 fu fatto il <:=spokendate('2023-06-10'):>."
/>
Questo rilascio contiene importanti cambiamenti descritti
nel <a href="$(HOME)/News/2023/20230610">comunicato stampa</a> e
nelle <a href="releasenotes">Note di rilascio</a>.</p>

# <p><strong>Debian 12 è stata sostituita da
# <a href="../bookworm/">Debian 13 (<q>trixie</q>)</a>.
# Gli aggiornamenti per la sicurezza sono stati interrotti 
# dal <:=spokendate('xxxx-xx-xx'):>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>Nonostante bookworm benefici anche del Supporto a Lungo Termine
# (LTS Long Term Support) fino al 30 giugno 2028. Tale supporto è limitato alle
# architetture i386, amd64, armel, armhf e arm64; tutte le altre architetture
# non hanno supporto. Per ulteriori informazioni fare riferimento alla <a
# href="https://wiki.debian.org/LTS">sezione LTS del Wiki Debian</a>.
# </strong></p>

<p>Per ottenere e installare Debian, si veda la pagina
con le <a href="debian-installer/">informazioni sull'installazione</a> e
la <a href="installmanual">Guida all'installazione</a>. Per aggiornare
da un precedente rilascio di Debian, consultare le
<a href="releasenotes">Note di rilascio</a>.</p>

### Activate the following when LTS period starts.
#<p>Architetture gestite nel periodo Long Term Support:</p>
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Architetture supportate al momento del rilascio iniziale di bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Nonstante la nostra volontà, questo rilascio potrebbe avere problemi,
anche se è chiamato <em>stable</em>. Esiste un <a href="errata">elenco
dei principali problemi conosciuti</a>, ed è possibile <a
href="../reportingbugs">segnalare altri problemi</a>.</p>

<p>Infine, ma non meno importante, è presente un elenco di
<a href="credits">persone da ringraziare</a> per aver permesso questo
rilascio.</p>
