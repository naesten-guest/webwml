#use wml::debian::template title="Informazioni sul rilascio Debian «jessie»"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ff4acdb89311338bebdbb71b28dcc479d29029e3" maintainer="Luca Monducci"

<p>Debian <current_release_jessie> è stata rilasciata <a
href="$(HOME)/News/<current_release_newsurl_jessie/>"><current_release_date_jessie></a>.
<ifneq "8.0" "<current_release>"
  "Debian 8.0 è stata inizialmente rilasciata il <:=spokendate('2015-04-26'):>."
/>
Il rilascio includeva molte modifiche significative, descritte nel nostro
<a href="$(HOME)/News/2015/20150426">comunicato stampa</a> e nelle <a
href="releasenotes">Note di Rilascio</a>.</p>

<p><strong>Debian 8 è stata sostituita da
<a href="../stretch/">Debian 9 (<q>stretch</q>)</a>.
Gli aggiornamenti di sicurezza sono stati interrotti dal
<:=spokendate('2018-06-17'):>.</strong></p>

<p><strong>Jessie ha anche beneficiato del Supporto a Lungo Termine (LTS)
fino alla fine di giugno 2020. Il LTS era limitato a i386, amd64, armel e
armhf. Per ulteriori informazioni, consultare la <a
href="https://wiki.debian.org/LTS">sezione LTS del Debian Wiki</a>.
</strong></p>

<p>Per ottenere e installare Debian, consulta la pagina delle informazioni
sull'installazione e la Guida all'installazione. Per effettuare 
l'aggiornamento da una versione precedente di Debian, vedi le istruzioni
nelle <a href="releasenotes">Note di Rilascio</a>.</p>

# Activate the following when LTS period starts.
<p>Architetture supportate durante il Supporto a Lungo Termine:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>

<p>Architetture supportate al rilascio iniziale di stretch:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
</ul>

<p>Contrariamente a quanto ci auguriamo, è possibile che esistano alcuni
problemi, nonostante sia dichiarata <em>stabile</em>. Abbiamo compilato <a
href="errata">una lista dei principali problemi noti</a>, e potete sempre
segnalarci altri problemi.</p>
