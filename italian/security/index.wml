#use wml::debian::template title="Security Information" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="452896bf2af61f852728d258a34c0609dd632373" maintainer="Luca Monducci"
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Mantenere sicuro il proprio sistema Debian</a></li>
<li><a href="#DSAS">Avvisi recenti</a></li>
<li><a href="#infos">Fonti di informazioni sulla sicurezza</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian prende molto sul
serio la sicurezza. Gestiamo tutti i problemi di sicurezza portati alla
nostra attenzione e ci assicuriamo che vengano corretti in tempi
ragionevoli.</p>
</aside>

<p>
L'esperienza ha dimostrato che la <q>sicurezza attraverso l'oscurità</q>
non funziona mai. Pertanto, la divulgazione pubblica consente di risolvere
più rapidamente e meglio i problemi di sicurezza. A questo proposito, questa
pagina affronta lo stato di Debian riguardo a varie falle di sicurezza note,
che potrebbero potenzialmente influenzare il sistema operativo Debian.
</p>

<p>
Il progetto Debian coordina molti avvisi di sicurezza con altri fornitori di
software libero e, di conseguenza, questi avvisi vengono pubblicati il giorno
stesso in cui una vulnerabilità viene resa pubblica.
Per ricevere gli ultimi avvisi di sicurezza Debian, iscriversi alla mailing
list <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Debian partecipa anche alle iniziative di standardizzazione della sicurezza:
</p>

<ul>
<li>I <a href="#DSAS">Debian Security Advisories</a> sono <a
href="cve-compatibility">Compatibili con CVE</a></li>

<li>Debian <a href="oval/">pubblica</a> i propri rapporti sulla sicurezza
utilizzando <a href="https://github.com/CISecurity/OVALRepo">Open
Vulnerability Assessment Language (OVAL)</a></li>
</ul>


<h2><a id="keeping-secure">Mantenere sicuro il proprio sistema Debian</a></h2>

<p>
È possibile installare il pacchetto <a
href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
per mantenere il computer aggiornato con gli
ultimi aggiornamenti di sicurezza (e di altro tipo) in modo automatico.

La <a href="https://wiki.debian.org/UnattendedUpgrades">pagina del wiki</a>
contiene informazioni più dettagliate su come impostare manualmente gli
<tt>unattended-upgrades</tt> (aggiornamenti non presidiati).
</p>

<p>
Per ulteriori informazioni sui problemi di sicurezza in Debian, consultare
le nostre FAQ e la nostra documentazione:
</p>

<p style="text-align:center">
<button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="faq">FAQ sulla Securezza</a></button>
<button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="../doc/user-manuals#securing">Securing Debian</a></button>
</p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>


<h2><a id="DSAS">Avvisi recenti</a> <a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>

<p>Questi sono i recenti avvisi di sicurezza Debian (DSA) inviati alla lista <a
href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> è il collegamento alle informazioni del <a
href="https://security-tracker.debian.org/tracker">Debian Security
Tracker</a>, il numero DSA rimanda alla mail di annuncio.
</p>

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}


<h2><a id="infos">Fonti di informazioni sulla sicurezza</a></h2>
#include "security-sources.inc"
