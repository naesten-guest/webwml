#use wml::debian::template title="Debian trixie -- Installatiehandleiding" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#use wml::debian::translation-check translation="f91240733bc268037daa6b2ce540bdb22dae5ad2"

<if-stable-release release="bookworm">
<p><p>Dit is een <strong>bètaversie</strong> van de Installatiehandleiding voor
de nog niet uitgebrachte release Debian 13, met codenaam trixie. Het is
mogelijk dat de hier beschikbare informatie verouderd en/of incorrect is als
gevolg van wijzigingen in het installatiesysteem. Mogelijk bent u ook
geïnteresseerd in de
<a href="../bookworm/installmanual">Installatiehandleiding voor Debian
12, codenaam bookworm</a>, de laatst uitgebrachte versie van
Debian; of in de <a href="https://d-i.debian.org/manual/">ontwikkelingsversie
van de installatiehandleiding</a>, die de meest recente versie van dit document
is.</p>
</if-stable-release>

<p>Er zijn installatie-instructies en downloadbare bestanden beschikbaar voor
elk van de ondersteunde architecturen:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>Als u de lokalisatie van uw browser correct heeft ingesteld, dan kunt u
bovenstaande link gebruiken om automatisch de juiste HTML-versie te verkrijgen
&mdash; zie <a href="$(HOME)/intro/cn">onderhandeling over inhoud (content negotiation)</a>.
Kies anders uit onderstaande tabel exact uw gewenste architectuur, taal en bestandsformaat.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architectuur</strong></th>
  <th align="left"><strong>Bestandsformaat</strong></th>
  <th align="left"><strong>Taal</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
