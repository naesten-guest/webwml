#use wml::debian::template title="Debian &ldquo;wheezy&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="fc0a147ef1585beaa5ef80938ca7e595d27fa365"



<p>Debian <current_release_wheezy> werd uitgebracht op <a href="$(HOME)/News/<current_release_newsurl_wheezy/>"><current_release_date_wheezy></a>.
<ifneq "7.0" "<current_release>"
  "Debian 7.0 werd oorspronkelijk uitgebracht op <:=spokendate('2013-05-04'):>."
/>
De release bevatte verschillende belangrijke wijzigingen, beschreven in ons
<a href="$(HOME)/News/2013/20130504">persbericht</a> en in de
<a href="releasenotes">Notities bij de release</a>.</p>

<p><strong>Debian 7 werd vervangen door
<a href="../jessie/">Debian 8 (<q>jessie</q>)</a>.
# Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('XXXXXXXXXXX'):>.
</strong></p>

<p><strong>Wheezy heeft ook genoten van langetermijnondersteuning (Long Term Support - LTS) tot
eind Mei 2018. De LTS was beperkt tot i386, amd64, armel en armhf.
Raadpleeg voor meer informatie de <a
href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
</strong></p>

<p>Raadpleeg de installatie-informatie-pagina en de
Installatiehandleiding over het verkrijgen en installeren
van Debian. Zie de instructies in
de <a href="releasenotes">Notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>

<p>De volgende computerarchitecturen worden in deze release ondersteund:</p>

<ul>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD 64-bits pc (amd64)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD 32-bits pc (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd andere problemen rapporteren.</p>
