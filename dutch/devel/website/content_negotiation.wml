#use wml::debian::template title="Onderhandeling over inhoud - Content Negotiation"
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc"

<H3>Hoe weet de server welk bestand moet worden weergegeven?</H3>
<P>U zult merken dat interne links niet eindigen op .html. Dit
is omdat de server inhoudsonderhandeling (content negotiation) gebruikt om te
beslissen welke versie van het document moet worden afgeleverd. Wanneer er
meer dan één keuze is, maakt de server een lijst van alle mogelijke
weer te geven bestanden, bijv. als de pagina 'partners' aangevraagd wordt,
dan zou de aangevulde lijst partners.en.html en partners.de.html kunnen zijn.
Het standaardgedrag van de servers van Debian is het Engelse document
weer te geven, maar dit kan geconfigureerd worden.

<P>Indien bij de clientbrowser de passende variabele ingesteld is, bijvoorbeeld
om bediend te worden met de Duitstalige pagina, dan zal in het bovenstaande
voorbeeld partners.de.html geleverd worden. Het leuke aan deze opzet is
dat als de gewenste taal niet beschikbaar is, er in plaats daarvan een
andere taal wordt aangeleverd (wat hopelijk beter is dan niets). De
beslissing over welk document geleverd wordt, is een beetje verwarrend.
In plaats van dit hier te beschrijven, kunt u het definitieve antwoord
vinden op <a href="https://httpd.apache.org/docs/current/content-negotiation.html">https://httpd.apache.org/docs/current/content-negotiation.html</a> indien u daarin geïnteresseerd bent.

<P>Omdat veel gebruikers zelfs niet eens op de hoogte zijn van het bestaan
van inhoudsonderhandeling, zijn er onderaan elke pagina links die verwijzen
naar elke andere beschikbare taal. Deze worden berekend door een perlscript
dat door wml aangeroepen wordt op het moment dat de pagina gegenereerd wordt.

<P>Er bestaat ook een optie om de taalvoorkeuren van de browser te
doorbreken met behulp van een cookie die de voorkeur
geeft aan één enkele taal in plaats van aan de browservoorkeuren.
