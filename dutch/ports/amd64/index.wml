#use wml::debian::template title="AMD64-port"
#use wml::debian::translation-check translation="c31fd1581b29fe64394cf736d6444851bba82a0f"


#use wml::debian::toc

<toc-display/>

<toc-add-entry name="about">Debian op AMD64</toc-add-entry>
<p>Deze pagina is bedoeld ter ondersteuning van gebruikers en ontwikkelaars
van Debian die Debian GNU/Linux gebruiken op de AMD64-architectuur. Hier
vindt u informatie over de huidige status van deze port, welke machines
publiek toegankelijk zijn voor ontwikkelaars, waar de ontwikkeling besproken
wordt, waar bijkomende informatie te vinden is over ontwikkelaars die
Debian geschikt maken voor deze architectuur en andere verwijzingen
naar extra informatie.</p>

<toc-add-entry name="status">Huidige status</toc-add-entry>
<p>AMD64 is sinds de release van Debian 4.0 (etch) een officieel ondersteunde
Debian-architectuur.
</p>

<p>De port bestaat uit een kernel voor alle AMD 64-bits CPU's met <em>AMD64</em>
als extensie en alle Intel CPU's met <em>Intel 64</em> als extensie, en een
gemeenschappelijke 64-bits gebruikersruimte.</p>

<toc-add-entry name="features">Een volledige 64-bits gebruikersomgeving</toc-add-entry>
<p>De port AMD64 is door en door 64-bits en laat de gebruiker genieten van alle
voordelen van deze architectuur, vergeleken bij de i386:
</p>
<ul>
<li>geen geheugenopdeling in laag en hoog geheugen</li>
<li>tot 128 TiB virtuele adresruimte per proces (in plaats van 2 GiB)</li>
<li>ondersteuning voor 64 TiB fysiek geheugen in plaats van 4 GiB (of 64 GiB met de PAE-extensie)</li>
<li>16 algemene registers in het CPU in plaats van 8</li>
<li>gcc gebruikt voor berekeningen standaard de SSE2-instructieset in plaats van 387 FPU</li>
<li>gcc laat standaard frame-pointers weg bij -O2</li>
<li>de compilatietijd-optimalisatie maakt gebruik van de gemeenschappelijke
basis voor AMD64/Intel 64 en niet van de verouderde i386-rommel</li>
<li>geheugenpagina's zijn standaard niet-uitvoerbaar</li>
</ul>

<p>De kernel ondersteunt een natuurlijke uitvoering van oudere 32-bits
programma's en in de benodigde kernbibliotheken wordt voorzien door Debian's <a
href="https://wiki.debian.org/Multiarch">Multiarchitectuurmechanisme</a>.</p>


<toc-add-entry name="i386support">Minimalistische AMD64 runtime-ondersteuning in de i386</toc-add-entry>
<p>De officiële i386-distributie bevat momenteel minimalistische AMD64-ondersteuning,
bestaande uit een 64-bits kernel, een gereedschapsset die 64-bits uitvoerbare
bestanden kan creëren en het pakket amd64-libs om amd64-programma's van
derden uit te voeren met systeemeigen gedeelde bibliotheken.</p>


<toc-add-entry name="ml">Mailinglijsten</toc-add-entry>

<p>De ontwikkeling van deze port wordt besproken op de lijst <a
href="https://lists.debian.org/debian-amd64/">debian-amd64</a>.</p>


<toc-add-entry name="publicmachines">Publieke machines</toc-add-entry>

<p>
Alle leden van Debian kunnen de zogenaamde
<a href="https://db.debian.org/machines.cgi">porterbox-machines</a>
van Debian gebruiken om pakketten geschikt te maken voor deze architectuur.
</p>

<toc-add-entry name="links">Links</toc-add-entry>

<ul>
<li><a href="https://wiki.debian.org/DebianAMD64">de wiki-pagina's van debian-amd64</a></li>
</ul>

