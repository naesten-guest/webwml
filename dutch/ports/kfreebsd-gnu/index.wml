#use wml::debian::template title="Debian GNU/kFreeBSD"

#use wml::debian::toc
#use wml::debian::translation-check translation="1b39a9c0d2369a1c450d27f42462c2dc55c8aa37"

<toc-display/>

<p>
Debian GNU/kFreeBSD is een debian-uitgave die bestaat uit het
<a href="https://www.gnu.org/">GNU userland</a> en de
<a href="https://www.gnu.org/software/libc">GNU C-bibliotheek</a>, bovenop de
kernel van <a href="http://www.freebsd.org">FreeBSD</a>, met daarbij de
gebruikelijke <a href="https://packages.debian.org/">pakketten van Debian</a>.
</p>

<div class="important">
De ontwikkeling van Debian GNU/kFreeBSD is officieel beëindigd in juli 2023 vanwege een gebrek aan interesse en vrijwilligers. U kunt de
<a href="https://lists.debian.org/debian-devel/2023/07/msg00176.html">officiële
aankondiging hier</a> vinden.
</div>

<div class="important">
<p>
Debian GNU/kFreeBSD is geen officieel ondersteunde architectuur.
Ze werd met Debian 6.0 (Squeeze) en 7.0 (Wheezy) uitgebracht
als een <i>technology preview</i> en de eerste niet-Linux uitgave van Debian.
Sinds Debian 8 (Jessie) behoort ze echter niet langer tot de officiële releases.
</p>
</div>


<toc-add-entry name="resources">Informatiebronnen</toc-add-entry>

<p>
Er is meer informatie beschikbaar over de uitgave (inclusief een FAQ) op de
wikipagina van <a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian
GNU/kFreeBSD</a>.
</p>

<h3>E-maillijsten</h3>

<p>
De <a href="https://lists.debian.org/debian-bsd">Debian GNU/k*BSD
e-mailijst</a>.
</p>

<h3>IRC</h3>

<p>
Het <a href="irc://irc.debian.org/#debian-kbsd">#debian-kbsd IRC-kanaal</a> (op
irc.debian.org).
</p>


<toc-add-entry name="Development">Ontwikkeling</toc-add-entry>

<p>
Omdat we gebruik maken van Glibc zijn de overzettingsproblemen erg eenvoudig
en meestal is het een kwestie van de testgevallen voor ""k*bsd*-gnu" van een
ander Glibc-systeem te kopiëren (zoals GNU of GNU/Linux). Bekijk het <a
href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">overzettingsdocument</a>
voor details.
</p>

<p>
Bekijk ook de <a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">takenlijst</a>
voor details over wat er moet gebeuren.
</p>


<toc-add-entry name="availablehw">Beschikbare hardware voor Debian
ontwikkelaars</toc-add-entry>

<p>
lemon.debian.net (kfreebsd-amd64) is
beschikbaar voor Debian ontwikkelaars voor overzettingsdoeleinden. Zie de <a
href="https://db.debian.org/machines.cgi">machine-database</a> voor meer
informatie over deze machines. Over het algemeen heeft u de beschikking over
twee chroot-omgevingen: testing en unstable. Let op: Deze systemen worden
niet beheerd door de DSA, dus <strong>stuur geen vragen over deze machines naar
debian-admin</strong>. Gebruik hiervoor <email "admin@lemon.debian.net">.
</p>
