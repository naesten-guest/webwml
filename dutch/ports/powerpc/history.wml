#use wml::debian::template title="Overzetting naar PowerPC -- Geschiedenis" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
#use wml::debian::translation-check translation="70cf45edbaeb4b8fc8f99d683f2f5c5c4435be92"

<br>
<br>

<h1>De geschiedenis van Debian/PowerPC</h1>

<p>
 De overzetting naar PowerPC startte in 1997 na het Duitse <a
 href="http://www.linux-kongress.org/">Linux Kongress</a> in
 W&uuml;rzburg waar het Debian-project gesponsord werd met een <a
 href="http://www.infodrom.north.de/Infodrom/tervola.html">PowerPC</a>
 voor ontwikkelingsdoeleinden.
</p>

<p>
 Gelukkig werden werkende boot- en rootdiskettes gevonden op <a
 href="http://www.linuxppc.org/">LinuxPPC</a> en konden we proberen
 iets te installeren op de machine. Helaas waren hiervoor enkele programma's
 nodig die alleen onder Mac OS werkten. De enige manier om ze te installeren
 was om een andere machine met Mac OS te nemen waarop ze al geïnstalleerd
 waren. Aangezien gegevensuitwisseling met andere niet-Mac OS machines alleen
 mogelijk was via msdos-geformatteerde floppies, was dit een kip-en-ei-probleem.
</p>

<p>
 Op de een of andere manier slaagden we erin een andere schijf aan de machine
 te koppelen en Linux erop te installeren. Dat was Apple's beroemde DR 1.
 Plotseling begonnen we dpkg en vrienden over te zetten naar het nieuwe
 systeem. En, wauw, dat bleek mogelijk. Het was indrukwekkend hoe de eerste
 pakketten werden overgezet en gebouwd. Helaas kwam DR one niet met een
 dynamische linker en gedeelde bibliotheken. Een ander probleem was dat de
 bibliotheken en header-bestanden enigszins verouderd bleken en we niet in staat
 waren om automatisch een nieuwe libc te compileren. Het grootste en meest
 ernstige probleem waren echter die verdachte crashes die ook het
 bestandssysteem volledig verknoeiden en die alleen op te lossen waren door een
 herinstallatie.
</p>

<p>
 Klee Dienes, ook een ontwikkelaar van Debian, werkte op dit probleem en
 kwam plots af met een <a
 href="ftp://ftp.infodrom.north.de/pub/Linux/linux-pmac/debian/mklinuxfs.tar.gz">mklinux tar-archief</a>
 van een recenter systeem - een gehackte Debian GNU/Linux. Dit tar-archief
 bevatte de oude versie 1.99 van libc. Vanaf dat moment functioneerde de
 machine stabiel op het netwerk en konden we onze inspanningen voortzetten.
 We compileerden veel pakketten en merkten dat sommige header-bestanden niet
 correct waren en dat veel programma's niet konden worden gecompileerd met de
 gewone compiler.
</p>

<p>
 En dus begon Joel Klecker, op dat ogenblik een nieuwe ontwikkelaar van Debian,
 te werken aan egcs en zijn compilatie op de PowerPC-machine. Nadat dit gedaan
 was, was de volgende stap het werken aan de huidige versie van libc. Het bleek
 dat onze versie 1.99 van libc niet compatibel was met de volgende versie,
 pre2.1. Die was nodig om de overzetting in een stabiele toestand te krijgen.
</p>

<p>
 In dit stadium schakelde Hartmut Koptein over van m68k naar de omzetting naar
 powerpc en begon ervoor te ontwikkelen. Ontwikkelingsversies van
 Debian/PowerPC waren beschikbaar in 1998 en 1999.
</p>

<p>
 De overzetting werd voor de eerste maal officieel uitgebracht met
 Debian GNU/Linux 2.2 ("potato"), in augustus 2000.
</p>

<p>
 De ontwikkeling van de overzetting gaat verder. Een build daemon werd opgezet
 op voltaire.debian.org, een PowerPC machine geschonken door Daniel Jacobowitz,
 ook een ontwikkelaar van Debian.
</p>
