#use wml::debian::translation-check translation="b827d01d6caf2b68e865bd2c4ff78fab56ab8eb4" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.2</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la deuxième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction amd64-microcode "Mise à jour du microcode inclus, y compris des corrections pour <q>AMD Inception</q> pour les processeurs AMD Zen4 [CVE-2023-20569]">
<correction arctica-greeter "Prise en charge de la configuration du thème du clavier à l'écran au moyen de gsettings de ArcticaGreeter ; utilisation de la disposition OSK <q>Compact</q> (à la place de Small) qui inclut des touches spéciales telles que le umlaut allemand ; correction de l'affichage des messages d'échec d'authentification ; utilisation du thème Active à la place d'Emerald">
<correction autofs "Correction d'une régression affectant l'accessibilité des hôtes double couche">
<correction base-files "Mise à jour pour cette version">
<correction batik "Correction de problèmes de contrefaçon de requête côté serveur [CVE-2022-44729 CVE-2022-44730]">
<correction boxer-data "Plus d'installation de https-everywhere pour Firefox">
<correction brltty "xbrlapi : pas de tentative de démarrage de brltty avec ba+a2 quand ce n'est pas disponible ; correction du déplacement du curseur et du défilement braille dans Orca quand xbrlapi est installé mais que le pilote d'écran a2 ne l'est pas">
<correction ca-certificates-java "Contournement de la non configuration de JRE lors des nouvelles installations">
<correction cairosvg "Gestion de données : URL en mode sûr">
<correction calibre "Correction de la fonction d'exportation">
<correction clamav "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-20197 CVE-2023-20212]">
<correction cryptmount "Problèmes d'initialisation de mémoire évités dans l'analyseur de ligne de commande">
<correction cups "Correction d'un problème de dépassement de tampon de tas [CVE-2023-4504] ; correction d'un problème d'accès non authentifié [CVE-2023-32360]">
<correction curl "Construction avec OpenLDAP pour corriger la récupération incorrecte des attributs binaires de LDAP ; correction d'un problème de consommation excessive de mémoire [CVE-2023-38039]">
<correction cyrus-imapd "Assurance que les boîtes aux lettres ne disparaissent pas lors des mises à niveau à partir de Bullseye">
<correction dar "Correction de problèmes lors de la création de catalogues isolés quand dar a été construit avec une version récente de gcc">
<correction dbus "Nouvelle version amont stable ; correction d'un plantage de dbus-daemon lors du rechargement de la politique lorsqu'une connexion appartient à un compte utilisateur qui a été supprimé, ou si un greffon NSS (Name Service Switch) est cassé, sur les noyaux qui ne prennent pas en charge SO_PEERGROUPS ; rapport d'erreur correct quand l'obtention des groupes d'un UID échoue ; dbus-user-session : copie de XDG_CURRENT_DESKTOP dans l'environnement d'activation">
<correction debian-archive-keyring "Nettoyage des trousseaux de clés inutilisés dans trusted.gpg.d">
<correction debian-edu-doc "Mise à jour du manuel de Debian Edu Bookworm">
<correction debian-edu-install "Nouvelle version amont ; ajustement des tailles des partitions automatiques de l’installateur Debian">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 6.1.0-13 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-parl "Reconstruction avec la nouvelle version de boxer-data ; plus de dépendance à webext-https-everywhere">
<correction debianutils "Correction d'entrées dupliquées dans /etc/shells ; gestion de /bin/sh dans le fichier d'état ; correction de la forme canonique des interpréteurs dans les emplacements des alias">
<correction dgit "Utilisation du mappage ancien /updates vers security seulement pour Buster ; chargement empêché des versions plus anciennes qui sont déjà dans l'archive">
<correction dhcpcd5 "Mises à niveau facilitée avec des programmes laissés par Wheezy ; abandon de l'intégration obsolète de ntpd ; correction de version dans le script de nettoyage">
<correction dpdk "Nouvelle version amont stable">
<correction dput-ng "Mise à jour des cibles de téléversement permises ; correction d'échec de construction à partir du paquet source">
<correction efibootguard "Correction d'une validation insuffisante ou absente et de nettoyage de l'entrée à partir de fichiers d'environnement d'un chargeur d'amorçage non sûr [CVE-2023-39950]">
<correction electrum "Correction d'un problème de sécurité de Lightning">
<correction filezilla "Correction de construction pour les architectures 32 bits ; correction d'un plantage lors du retrait de types de fichier d'une liste">
<correction firewalld "Pas de mélange d'adresses IPv4 et IPv6 dans une règle unique de nftables">
<correction flann "Abandon de la bibliothèque supplémentaire -llz4 dans flann.pc">
<correction foot "Requêtes XTGETTCAP avec un encodage hexadécimal non valable ignorées">
<correction freedombox "Utilisation de n= dans les préférences d'apt pour des mises à niveau sans problème">
<correction freeradius "Données correctes dans TLS-Client-Cert-Common-Name assurée">
<correction ghostscript "Correction d'un problème de dépassement de tampon [CVE-2023-38559] ; essai et sécurisation du démarrage du serveur IJS [CVE-2023-43115]">
<correction gitit "Reconstruction avec la nouvelle version de pandoc">
<correction gjs "Boucles infinies évitées des rappels de suspension si un gestionnaire de suspension est appelé durant l’utilisation du ramasse miettes (GC)">
<correction glibc "Correction de la valeur de F_GETLK/F_SETLK/F_SETLKW avec __USE_FILE_OFFSET64 sur ppc64el ; correction d'un dépassement de lecture de pile dans getaddrinfo en mode no-aaaa [CVE-2023-4527] ; correction d'une utilisation de mémoire après libération dans getcanonname [CVE-2023-4806 CVE-2023-5156] ; correction de _dl_find_object pour renvoyer des valeurs correctes même durant le début du démarrage">
<correction gosa-plugins-netgroups "Avertissements de dépréciation silencieux dans l'interface web">
<correction gosa-plugins-systems "Correction de la gestion des entrées DHCP/DNS dans le thème par défaut ; correction de l'ajout de systèmes (autonomes) <q>Imprimante réseau</q> ; correction de la génération de DNS cible pour divers types de systèmes ; correction du rendu des icônes dans le servlet DHCP ; nom d'hôte non qualifié appliqué pour les stations de travail">
<correction gtk+3.0 "Nouvelle version amont stable ; correction de plusieurs plantages ; plus d'informations montrées dans l'interface de débogage <q>inspector</q> ; avertissements silencieux de GFileInfo lors d'une utilisation d'une version de Glibc rétroportée ; utilisation d'une couleur claire pour le curseur pour les thèmes sombres, améliorant beaucoup sa visibilité dans certaines applications et en particulier Evince">
<correction gtk4 "Correction d'une troncature dans PlacesSidebar avec la configuration d'accessibilité des textes longs">
<correction haskell-hakyll "Reconstruction avec la nouvelle version de pandoc">
<correction highway "Correction de la prise en charge des systèmes armhf où NEON est absent">
<correction hnswlib "Correction d'une double libération de mémoire dans init_index quand l'argument M est un grand entier [CVE-2023-37365]">
<correction horizon "Correction d'un problème de redirection ouverte [CVE-2022-45582]">
<correction icingaweb2 "Suppression de notices de dépréciation indésirable">
<correction imlib2 "Correction de la préservation de l'attribut de canal alpha">
<correction indent "Correction d'une lecture hors tampon ; correction d'écrasement de tampon [CVE-2023-40305]">
<correction inetutils "Vérification des valeurs de retour lors de l'abandon de privilèges [CVE-2023-40303]">
<correction inn2 "Correction des blocages de nnrpd quand la compression est activée ; ajout de la prise en charge des horodatages de haute précision pour syslog ; inn-{radius,secrets}.conf rendus non lisibles par tous">
<correction jekyll "Prise en charge des alias YAML">
<correction kernelshark "Correction d'une erreur de segmentation dans libshark-tepdata ; correction de la capture quand le répertoire cible contient un espace">
<correction krb5 "Correction de libération d'un pointeur non initialisé [CVE-2023-36054]">
<correction lemonldap-ng "Application du contrôle d'identifiant pour les requêtes auth-slave ; correction d'une redirection ouverte due au traitement incorrect d'un échappement ; correction d'une redirection ouverte quand OIDC RP n'a pas d'URI de redirection ; correction d'un problème de contrefaçon de requête côté serveur [CVE-2023-44469]">
<correction libapache-mod-jk "Suppression de la fonctionnalité de mappage implicite qui pouvait mener à l'exposition imprévue de l'état <q>worker</q> et/ou <q>bypass</q> des contraintes de sécurité [CVE-2023-41081]">
<correction libclamunrar "Nouvelle version amont stable">
<correction libmatemixer "Correction de corruptions de tas ou de plantages des applications lors du retrait de périphériques audio">
<correction libpam-mklocaluser "pam-auth-update : assurance que le module est ordonnancé avant les autres modules de type session">
<correction libxnvctrl "Nouveau paquet source séparé de nvidia-settings">
<correction linux "Nouvelle version amont stable">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction llvm-defaults "Correction du lien symbolique /usr/include/lld ; ajout de Breaks sur les paquets non co-installables pour des mises à niveau sans problème à partir de Bullseye">
<correction ltsp "Utilisation évitée de mv sur le lien symbolique init">
<correction lxc "Correction de la syntaxe de nftables pour la traduction d'adresse réseau pour les paquets IPv6">
<correction lxcfs "Correction du rapport de processeur dans un conteneur arm32 avec un grand nombre de processeurs">
<correction marco "Activation de la composition seulement si elle est disponible">
<correction mariadb "Nouvelle version amont de correction de bogues">
<correction mate-notification-daemon "Correction de deux fuites de mémoire">
<correction mgba "Correction du son cassé dans libretro core ; correction d'un plantage sur le matériel ignorant OpenGL 3.2">
<correction modsecurity "Correction d'un problème de déni de service [CVE-2023-38285]">
<correction monitoring-plugins "check_disk : montage évité lors de la recherche de point des montages correspondants, résolvant une régression de vitesse depuis Bullseye">
<correction mozjs102 "Nouvelle version amont stable ; correction de <q>valeur incorrecte utilisée durant la compilation de WASM</q> [CVE-2023-4046], problème potentiel d'utilisation de mémoire après libération [CVE-2023-37202], problème de sécurité mémoire [CVE-2023-37211 CVE-2023-34416]">
<correction mutt "Nouvelle version amont stable">
<correction nco "Réactivation de la prise en charge de udunits2">
<correction nftables "Correction de la génération incorrecte de bytecode touchée par la nouvelle vérification du noyau qui rejette l'ajout de règles à la chaîne de liens">
<correction node-dottie "Correction de sécurité (pollution de prototype) [CVE-2023-26132]">
<correction nvidia-settings "Nouvelle version amont de correction de bogues">
<correction nvidia-settings-tesla "Nouvelle version amont de correction de bogues">
<correction nx-libs "Correction de l'absence du lien symbolique /usr/share/nx/fonts ; correction de la page de manuel">
<correction open-ath9k-htc-firmware "Chargement du microprogramme correct">
<correction openbsd-inetd "Correction de problèmes de gestion de mémoire">
<correction openrefine "Correction d'un problème d'exécution de code arbitraire [CVE-2023-37476]">
<correction openscap "Correction des dépendances à openscap-utils et python3-openscap">
<correction openssh "Correction d'un problème d'exécution de code à distance au moyen d'un socket d'agent transmis [CVE-2023-38408]">
<correction openssl "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-2975 CVE-2023-3446 CVE-2023-3817]">
<correction pam "Correction de pam-auth-update --disable ; mise à jour de la traduction en turc">
<correction pandoc "Correction d'un problème d'écriture d'un fichier arbitraire [CVE-2023-35936]">
<correction plasma-framework "Correction de plantages de plasmashell">
<correction plasma-workspace "Correction d'un plantage dans krunner">
<correction python-git "Correction d'un problème d'exécution de code à distance [CVE-2023-40267], d'un problème d'inclusion inaperçue de fichier local [CVE-2023-41040]">
<correction pywinrm "Correction de compatibilité avec Python 3.11">
<correction qemu "Mise à jour vers l'arbre 7.2.5 amont ; ui/vnc-clipboard : correction d'une boucle infinie dans inflate_buffer [CVE-2023-3255] ; correction d'un problème de déréférencement de pointeur NULL [CVE-2023-3354] ; correction d'un problème de dépassement de tampon [CVE-2023-3180]">
<correction qtlocation-opensource-src "Correction de gel lors du chargement de tuiles de cartes">
<correction rar "Version amont de correction de bogues [CVE-2023-40477]">
<correction reprepro "Correction d'une situation de compétition lors de l'utilisation de décompresseurs externes">
<correction rmlint "Correction d'erreur dans d'autres paquets provoquée par une version erronée du paquet python ; correction d'échec de démarrage de l'interface graphique avec les versions récentes de Python 3.11">
<correction roundcube "Nouvelle version amont stable ; correction de l'authentification OAuth2 ; corrections de problèmes de script intersite [CVE-2023-43770]">
<correction runit-services "dhclient : pas d'utilisation d'eth1 codé en dur ">
<correction samba "Nouvelle version amont stable">
<correction sitesummary "Nouvelle version amont ; correction de l'installation du script sitesummary-maintenance de CRON/systemd-timerd ; correction de la création non sûre de fichiers et de répertoires temporaires">
<correction slbackup-php "corrections de bogues : journalisation des commandes distantes vers stderr ; désactivation de fichiers d'hôtes SSH connus ; compatibilité avec PHP 8">
<correction spamprobe "Correction de plantage dans l'analyse de pièces jointes JPEG">
<correction stunnel4 "Correction de la fermeture par un pair d'une connexion TLS sans envoi d'un message correct de fermeture">
<correction systemd "Nouvelle version amont stable ; correction d'un problème de sécurité mineur dans systemd-boot (EFI) d'arm64 et de riscv64 avec le chargement de l'arbre des périphériques (device-tree blob)">
<correction testng7 "Rétroportage dans stable pour les constructions futures d'openjdk-17">
<correction timg "Correction d'une vulnérabilité de dépassement de tampon [CVE-2023-40968]">
<correction transmission "Remplacement du correctif de compatibilité avec openssl3 pour corriger une fuite de mémoire">
<correction unbound "Correction de saturation du journal d'erreurs lors de l'utilisation de DNS sur TLS avec openssl 3.0">
<correction unrar-nonfree "Correction d'un problème d'exécution de code à distance [CVE-2023-40477]">
<correction vorta "Gestion des modifications de ctime et mtime dans les fichiers diff">
<correction vte2.91 "<q>Ring view</q> invalidé plus souvent quand c'est nécessaire, correction de divers échecs d'assertion durant la gestion d'événements">
<correction x2goserver "x2goruncommand : ajout de la prise en charge de KDE Plasma 5 ; x2gostartagent : corruption de fichier journal empêchée ; keystrokes.cfg : synchronisation avec nx-libs ; correction de la traduction en finnois">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5454 kanboard>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5458 openjdk-17>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5460 curl>
<dsa 2023 5462 linux-signed-amd64>
<dsa 2023 5462 linux-signed-arm64>
<dsa 2023 5462 linux-signed-i386>
<dsa 2023 5462 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5466 ntpsec>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5469 thunderbird>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5477 samba>
<dsa 2023 5479 chromium>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5487 chromium>
<dsa 2023 5488 thunderbird>
<dsa 2023 5491 chromium>
<dsa 2023 5492 linux-signed-amd64>
<dsa 2023 5492 linux-signed-arm64>
<dsa 2023 5492 linux-signed-i386>
<dsa 2023 5492 linux>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5497 libwebp>
<dsa 2023 5498 thunderbird>
<dsa 2023 5501 gnome-shell>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction https-everywhere "Obsolète, les principaux navigateurs proposent une prise en charge native">

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
