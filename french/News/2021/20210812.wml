#use wml::debian::translation-check translation="a48c50faef33185d0ae2746854bc99435817bad1" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Le projet Debian déplore la perte de Robert Lemmen, Karl Ramm et Rogério Theodoro de Brito</define-tag>
<define-tag release_date>2021-08-12</define-tag>
#use wml::debian::news
# $Id$

<p>Durant cette dernière année, le projet Debian a perdu plusieurs membres de sa communauté.</p>

<p>
En Juin 2020, Robert Lemmen est décédé des suites d'une grave maladie. Robert
assistait régulièrement aux rencontres Debian de Munich depuis le début des
années 2000 et contribuait aux stands locaux. Il a été développeur Debian
à partir de 2007. Entre autres contributions, il empaquetait des modules pour
Raku (anciennement Perl 6) et aidait d'autres contributeurs à s'impliquer dans
l'équipe de Raku. Il s'employait aussi à traquer les dépendances circulaires
dans Debian.
</p>

<p>
Karl Ramm a succombé en juin 2020 à des complications dues à un cancer du côlon.
Développeur Debian depuis 2001, il empaquetait plusieurs composantes du
<a href="https://en.wikipedia.org/wiki/Project_Athena">Project Athena</a> du MIT.
Il était passionné par la technologie et Debian et était toujours intéressé à
aider les autres à découvrir et à promouvoir leurs passions.
</p>

<p>
En avril 2021, nous avons perdu Rogério Theodoro de Brito à cause de la
pandémie de la COVID-19. Rogério aimait programmer de petits outils et a été
contributeur Debian pendant plus de quinze ans. Entre autres projets, il a
contribué à l'utilisation des périphériques Kurobox et Linkstation dans Debian
et maintenait l'outil youtube-dl. Il a aussi participé à plusieurs projets
amont où il était le <q>contact Debian</q>.
</p>

<p>Le projet Debian rend hommage à Robert, Karl et Rogerio pour leur travail et
leur fort attachement à Debian et aux logiciels libres. Nous n'oublierons
jamais leurs contributions et la grande qualité de leur travail continuera à
nous inspirer.</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian est une association de développeurs de logiciels
libres qui offrent volontairement leur temps et leurs efforts pour
produire le système d'exploitation complètement libre Debian.
</p>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
