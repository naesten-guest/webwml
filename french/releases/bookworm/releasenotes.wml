#use wml::debian::translation-check translation="7727fd8734bae9f8383826260a368f991491f565" maintainer="Baptiste Jammet"
#use wml::debian::template title="Debian 12 – Notes de publication" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"

<p>Pour découvrir les nouveautés présentes dans Debian 12, consultez les notes
de publication pour votre architecture :</p>

<ul>
<:= &permute_as_list('release-notes/', 'Notes de publication'); :>
</ul>

<p>Les notes de publication contiennent également des instructions pour les
utilisateurs qui mettent à jour leur système à partir des versions précédentes.</p>

<p>Si vous avez configuré correctement les options de langue de votre
navigateur, vous pouvez utiliser le lien ci-dessus pour avoir automatiquement la
bonne page HTML &mdash; voir les explications concernant la <a href="$(HOME)/intro/cn">négociation de contenu</a>.
Sinon, choisissez l'architecture adéquate, la langue et le format que vous
souhaitez dans le tableau ci-dessous.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architecture</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Langues</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
