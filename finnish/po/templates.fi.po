msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"PO-Revision-Date: 2016-05-16 11:36+0300\n"
"Last-Translator: Tommi Vainikainen <tvainika@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Debian-sivusto"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Etsi Debianin www-sivustolta."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Debian-sivuston haku"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Kyllä"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Ei"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Debian-projekti"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian GNU/Linux on GNU/Linux-käyttöjärjestelmä ja vapaiden ohjelmistojen "
"jakelukokoelma.  Sitä ylläpitää ja uudistaa joukko vapaaehtoisia käyttäjiä."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, vapaa, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Takaisin <a href=\"m4_HOME/\">Debian-projektin kotisivulle</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Koti"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Ohita pikanavigointi"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Tietoa"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Tietoa Debianista"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Ota yhteyttä"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "Julkaisutiedot"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Lahjoitukset"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Tapahtumia"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Uutisia"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Jakelu"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Tuki"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Puhtaat sekoitukset"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Kehittäjien nurkkaus"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Käyttöohjeet"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Tietoturvallisuudesta"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Haku"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "ei&nbsp;mitään"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Etene"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "maailmanlaajuinen"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Sivustokartta"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Sekalaista"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Debianin hankkiminen"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Debian-blogi"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
#| msgid "Debian Project News"
msgid "Debian Micronews"
msgstr "Debianin projektikatsaus"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "Debian-projekti"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "Viimeksi muutettu"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Näitä sivuja koskevia Kommentteja, kritiikkiä ja ehdotuksia otetaan "
"mielellään vastaan <a href=\"mailto:debian-doc@lists.debian.org"
"\">postilistallamme</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "ei tarpeen"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "ei saatavilla"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/A"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "versiossa 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "versiossa 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "versiossa 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "versiossa 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "versiossa 2.2"

#: ../../english/template/debian/footer.wml:84
msgid ""
"See our <a href=\"m4_HOME/contact\">contact page</a> to get in touch. Web "
"site source code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">available</a>."
msgstr ""

#: ../../english/template/debian/footer.wml:87
msgid "Last Modified"
msgstr "Viimeksi muutettu"

#: ../../english/template/debian/footer.wml:90
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:93
msgid "Copyright"
msgstr "Tekijänoikeudet"

#: ../../english/template/debian/footer.wml:96
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> ja muut;"

#: ../../english/template/debian/footer.wml:99
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "Katso <a href=\"m4_HOME/license\" rel=\"copyright\">lisenssiehtoja</a>"

#: ../../english/template/debian/footer.wml:102
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian on Software in the Public Interest, Inc.'in rekisteröimä <a href="
"\"m4_HOME/trademark\">tavaramerkki</a>."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Tämä sivu on olemassa myös seuraavilla kielillä:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Oletuskielen <a href=m4_HOME/intro/cn>asettamisohjeet</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Kansainvälinen Debian"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Kumppanit"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Debianin viikkokatsaus"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Viikkokatsaus"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Debianin projektikatsaus"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Projektikatsaus"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Julkaisutiedot"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Debian-paketit"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Hakeminen"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;CD:llä"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Debian-kirjallisuutta"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Debian-wiki"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Postilistat"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Yhteisösopimus"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Menettelytavat"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - Kansainvälinen käyttöjärjestelmä"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Debian-sivujen sivustokartta"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Kehittäjätietokanta"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "Debian-VUKK"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Debianin linjan kuvaus"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Kehittäjien käsikirja"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Uuden ylläpitäjän opas"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Julkaisun estävät viat"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Lintian-raportit"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Käyttäjien postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Kehittäjien postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "I18n/l10n-postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Siirrosten postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Vianseurantajärjestelmän postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Muiden postilistojen arkistot"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Vapaa ohjelmisto"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Kehitys"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Auta Debiania"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Vikailmoitukset"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Siirrokset/arkkitehtuurit"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Asennusopas"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "CD-toimittajat"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "CD/USB-ISO-vedokset"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Verkkoasennus"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Esiasennettu"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Debian-Edu-projekti"

#: ../../english/template/debian/links.tags.wml:137
#, fuzzy
#| msgid "Alioth &ndash; Debian GForge"
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Laadunvalvonta"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Paketinseurantajärjestelmä"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Debian-kehittäjien pakettikatsaus"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Debian-koti"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Ei otsikoita tältä vuodelta"

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "ehdotettu"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "keskusteltavana"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "äänestys avoinna"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "päätetty"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "vedetty pois"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Tulevia tapahtumia"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Menneitä tapahtumia"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(uusi versio)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Kertomus"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:14
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s %s-arkkitehtuurille"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Huomaa:</em> <a href=\"$link\">Alkuperäinen</a> sivu on tätä suomennosta "
"uudempi."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Varoitus! Tämä suomennos on liian vanha, katso mieluummin <a href=\"$link"
"\">alkuperäistä sivua</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""
"<em>Huomaa:</em> Alkuperäistä sivua, josta tämä on käännetty, ei enää ole."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Takaisin <a href=\"../\">Ketkä käyttävät Debiania?-sivulle</a>."

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, versio %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "%s days in adoption."
#~ msgstr "%s päivää adoptiossa."

#~ msgid "%s days in preparation."
#~ msgstr "%s päivää valmistelussa."

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr ""
#~ "Uutiskirjeen <a href=\"../../\">vanhoja numeroita</a> on myös saatavilla."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (poistettu osoite)"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Projekti</th><th>Koordinaattori</th>"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Kansikuvia"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Lataa verkosta"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Peilaukset"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Sekalaista"

#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "<void id=\"dc_pik\" />Pseudo Image Kit"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Vedosten julkaisutiedotteet"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Rsync-peilit"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Lataa torrentilla"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />vukk"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />sekalaista"

#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debianin projektikatsausta toimittavat <a href=\"mailto:debian-"
#~ "publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debianin viikkokatsausta toimittavat <a href=\"mailto:dwn@debian.org\">"
#~ "%s</a>."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "Tämän numeron käänsivät %s."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Tämän Debianin projektikatsauksen toimittivat <a href=\"mailto:debian-"
#~ "publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debianin viikkokatsausta toimittavat <a href=\"mailto:dwn@debian.org\">"
#~ "%s</a>."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "Tämän numeron käänsivät %s."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debianin projektikatsausta toimittaa <a href=\"mailto:debian-"
#~ "publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debianin viikkokatsausta toimittaa <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "Tämän numeron käänsi %s."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Tämän Debianin projektikatsauksen toimitti <a href=\"mailto:debian-"
#~ "publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debianin viikkokatsausta toimittaa <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "Tämän numeron käänsi %s."

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Täydentää&nbsp;ehdotusta"

#~ msgid "Back to other <a href=\"./\">Debian news</a>."
#~ msgstr "Takaisin muihin <a href=\"./\">Debian-uutisiin</a>."

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Takaisin <a href=\"./\">Debian-konsulttisivulle</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Takaisin <a href=\"./\">Debian-puhujasivulle</a>."

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Takaisin: muihin <a href=\"./\">Debian-uutisiin</a> || <a href=\"m4_HOME/"
#~ "\">Debian-projektin kotisivulle</a>."

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Osta CD:t/DVD:t"

#~ msgid "Choices"
#~ msgstr "Vaihtoehdot"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG-VUKK"

#~ msgid "DLS Index"
#~ msgstr "DLS-hakemisto"

#~ msgid "Date"
#~ msgstr "Päiväys"

#~ msgid "Date published"
#~ msgstr "Julkistuspäivämäärä"

#~ msgid "Debian CD team"
#~ msgstr "Debian-CD-ryhmä"

#~ msgid "Debian Involvement"
#~ msgstr "Debianin osallisuus"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Debian-Legal-arkisto"

#~ msgid "Decided"
#~ msgstr "Päätetty"

#~ msgid "Discussion"
#~ msgstr "Keskusteltavana"

#~ msgid "Download calendar entry"
#~ msgstr "Imuroi kalenterimerkintä"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Lataa http/ftp:llä"

#~ msgid "Download with Jigdo"
#~ msgstr "Lataa jigdolla"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "Englanninkielinen <a href=\"/MailingLists/disclaimer\">julkinen "
#~ "postilista</a> CD/DVD:ille:"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Seurata&nbsp;ehdotuksen&nbsp;kehitystä"

#~ msgid "Free"
#~ msgstr "Vapaa"

#~ msgid "Have you found a problem with the site layout?"
#~ msgstr "Oletko havainnut ongelman sivuston ulkoasussa?"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Koti&nbsp;äänestyssivu"

#~ msgid "How&nbsp;To"
#~ msgstr "Kuinka"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Keskusteltavana"

#~ msgid "Justification"
#~ msgstr "Perustelu:"

#~ msgid "Latest News"
#~ msgstr "Tuoreimmat uutiset"

#~ msgid "License"
#~ msgstr "Lisenssi"

#~ msgid "License Information"
#~ msgstr "Lisenssitiedot"

#~ msgid "License text"
#~ msgstr "Lisenssiteksti"

#~ msgid "License text (translated)"
#~ msgstr "Lisenssiteksti (käännetty)"

#~ msgid "List of Consultants"
#~ msgstr "Konsulttilista"

#~ msgid "List of Speakers"
#~ msgstr "Lista puhujista"

#~ msgid "Main Coordinator"
#~ msgstr "Pääkoordinoija"

#~ msgid "Majority Requirement"
#~ msgstr "Enemmistövaatimus"

#~ msgid "More Info"
#~ msgstr "Lisätietoa"

#~ msgid "More information"
#~ msgstr "Lisätietoa"

#~ msgid "More information:"
#~ msgstr "Lisätietoa:"

#~ msgid "Network Install"
#~ msgstr "Verkkoasennus"

#~ msgid "No Requested packages"
#~ msgstr "Ei pyydettyjä paketteja"

#~ msgid "No help requested"
#~ msgstr "Ei pyydetty apua"

#~ msgid "No orphaned packages"
#~ msgstr "Ei orpoja paketteja"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "Ei paketteja odottamassa adoptiota"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "Ei paketteja odottamassa paketointia"

#~ msgid "No requests for adoption"
#~ msgstr "Ei adoptointipyyntöjä"

#~ msgid "Nobody"
#~ msgstr "Ei kukaan"

#~ msgid "Nominations"
#~ msgstr "Nimeämiset"

#~ msgid "Non-Free"
#~ msgstr "Epävapaa"

#~ msgid "Not Redistributable"
#~ msgstr "Ei jaeltavissa"

#~ msgid "Opposition"
#~ msgstr "Vastustus"

#~ msgid "Original Summary"
#~ msgstr "Alkuperäinen yhteenveto"

#~ msgid "Other"
#~ msgstr "Muu"

#~ msgid "Outcome"
#~ msgstr "Tulos"

#~ msgid "Proposal A"
#~ msgstr "Ehdotus A"

#~ msgid "Proposal A Proposer"
#~ msgstr "Ehdotuksen A ehdottaja"

#~ msgid "Proposal A Seconds"
#~ msgstr "Ehdotuksen A kannattajat"

#~ msgid "Proposal B"
#~ msgstr "Ehdotus B"

#~ msgid "Proposal B Proposer"
#~ msgstr "Ehdotuksen B ehdottaja"

#~ msgid "Proposal B Seconds"
#~ msgstr "Ehdotuksen B kannattajat"

#~ msgid "Proposal C"
#~ msgstr "Ehdotus C"

#~ msgid "Proposal C Proposer"
#~ msgstr "Ehdotuksen C ehdottaja"

#~ msgid "Proposal C Seconds"
#~ msgstr "Ehdotuksen C kannattajat"

#~ msgid "Proposal D"
#~ msgstr "Ehdotus D"

#~ msgid "Proposal D Proposer"
#~ msgstr "Ehdotuksen D ehdottaja"

#~ msgid "Proposal D Seconds"
#~ msgstr "Ehdotuksen D kannattajat"

#~ msgid "Proposal E"
#~ msgstr "Ehdotus E"

#~ msgid "Proposal E Proposer"
#~ msgstr "Ehdotuksen E ehdottaja"

#~ msgid "Proposal E Seconds"
#~ msgstr "Ehdotuksen E kannattajat"

#~ msgid "Proposal F"
#~ msgstr "Ehdotus F"

#~ msgid "Proposal F Proposer"
#~ msgstr "Ehdotuksen F ehdottaja"

#~ msgid "Proposal F Seconds"
#~ msgstr "Ehdotuksen F kannattajat"

#~ msgid "Proposer"
#~ msgstr "Ehdottaja"

#~ msgid "Rating:"
#~ msgstr "Aste:"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Lukea&nbsp;tulos"

#~ msgid "Related Links"
#~ msgstr "Aiheeseen liittyviä linkkejä"

#~ msgid "Report it!"
#~ msgstr "Ilmoita siitä!"

#~ msgid "Seconds"
#~ msgstr "Kannatukset"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Katso <a href=\"./\">lisenssitietojen</a> sivulta yleiskatsausta Debianin "
#~ "lisenssiyhteenvedoista (DLS)."

#~ msgid "Select a server near you"
#~ msgstr "Valitse läheinen palvelin"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Ehdottaa"

#~ msgid "Summary"
#~ msgstr "Yhteenveto"

#~ msgid "Taken by:"
#~ msgstr "Ottanut:"

#~ msgid "Text"
#~ msgstr "Teksti"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "Alkuperäinen yhteenveto, jonka valmisteli <summary-author/>, löytyy <a "
#~ "href=\"<summary-url/>\">lista-arkistoista</a>."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Tämän yhteenvedon valmisteli <summary-author/>."

#~ msgid "Time Line"
#~ msgstr "Aikajana"

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Saadaksesi tämän uutiskirjeen joka toinen viikko postilaatikkoosi, <a "
#~ "href=\"https://lists.debian.org/debian-news/\">liity debian-news-"
#~ "postilistalle</a>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Saadaksesi tämän uutiskirjeen viikottain postilaatikkoosi, <a href="
#~ "\"https://lists.debian.org/debian-news/\">liity debian-news-"
#~ "postilistalle</a>."

#, fuzzy
#~ msgid ""
#~ "To report a problem with the web site, please e-mail our publicly "
#~ "archived mailing list <a href=\"mailto:debian-www@lists.debian.org"
#~ "\">debian-www@lists.debian.org</a> in English.  For other contact "
#~ "information, see the Debian <a href=\"m4_HOME/contact\">contact page</a>. "
#~ "Web site source code is <a href=\"https://salsa.debian.org/webmaster-team/"
#~ "webwml\">available</a>."
#~ msgstr ""
#~ "Ilmoitukset www-sivustolla ilmenevästä ongelmasta voi lähettää "
#~ "(englanniksi) osoitteeseen <a href=\"mailto:debian-www@lists.debian.org"
#~ "\">debian-www@lists.debian.org</a>. Muut yhteystiedot löytyvät <a href="
#~ "\"m4_HOME/contact\">kontaktisivulta</a>. WWW-sivuston lähdekoodi on <a "
#~ "href=\"m4_HOME/devel/website/using_cvs\">saatavilla</a>."

#~ msgid "Upcoming Attractions"
#~ msgstr "Tulevia tapahtumia"

#~ msgid "Version"
#~ msgstr "Versio"

#~ msgid "Visit the site sponsor"
#~ msgstr "Vierailethan sivuston sponsorilla"

#~ msgid "Vote"
#~ msgstr "Äänestää"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Äänestys&nbsp;käynnissä"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Kannatusta odottavat"

#~ msgid "When"
#~ msgstr "Milloin"

#~ msgid "Where"
#~ msgstr "Missä"

#~ msgid "Withdrawn"
#~ msgstr "Vedetty pois"

#~ msgid "buy"
#~ msgstr "osta"

#~ msgid "buy pre-made images"
#~ msgstr "osta valmiit romput"

#~ msgid "debian_on_cd"
#~ msgstr "debian cd:llä"

#~ msgid "discussed"
#~ msgstr "keskusteltu"

#~ msgid "free"
#~ msgstr "vapaa"

#~ msgid "http_ftp"
#~ msgstr "http/ftp"

#~ msgid "in adoption since today."
#~ msgstr "adoptiossa tästä päivästä lähtien."

#~ msgid "in adoption since yesterday."
#~ msgstr "adoptiossa eilisestä lähtien."

#~ msgid "in preparation since today."
#~ msgstr "valmistelussa tästä päivästä lähtien."

#~ msgid "in preparation since yesterday."
#~ msgstr "valmistelussa eilisestä lähtien."

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "link may no longer be valid"
#~ msgstr "linkki voi olla vanhentunut"

#~ msgid "net_install"
#~ msgstr "verkkoasennus"

#~ msgid "network install"
#~ msgstr "verkkoasennus"

#~ msgid "non-free"
#~ msgstr "epävapaa"

#~ msgid "not redistributable"
#~ msgstr "ei jaeltavissa"

#~ msgid "package info"
#~ msgstr "pakettitietoa"

#~ msgid "requested %s days ago."
#~ msgstr "pyydetty %s päivää sitten."

#~ msgid "requested today."
#~ msgstr "pyydetty tänään."

#~ msgid "requested yesterday."
#~ msgstr "pyydetty eilen."
