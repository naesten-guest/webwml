#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"
#use wml::debian::translation-check translation="0ee4ac09253cee5e3e22c2621453da29a0526449"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>Yhteisö</h1>
      <h2>Debian on ihmisten yhteisö!</h2>
      
#include "$(ENGLISHDIR)/index.inc"

    <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Ihmiset</a></h2>
        <p>Keitä me olemme ja mitä me teemme</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Elämänkatsomuksemme</a></h2>
        <p>Miksi me teemme, ja miten me teemme</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Ota osaa, anna oma panoksesi</a></h2>
        <p>Näin liityt mukaan!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">More...</a></h2>
        <p>Lisätietoja Debian-yhteisöstä</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Käyttöjärjestelmä</h1>
      <h2>Debian on kaikenkattava vapaa käyttöjärjestelmä!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="$(HOME)/download" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Miksi Debian</a></h2>
        <p>Mikä tekee Debianista erityisen</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Tuki käyttäjälle</a></h2>
        <p>Avunsaanti ja dokumentaatio</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Turvapäivitykset</a></h2>
        <p>Debianin turvaneuvonannot - Debian Security Advisories (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">Lisää...</a></h2>
        <p>Lisälähteet latauksiin ja ohjelmistoihin</p>
      </div>
    </div>
  </div>
</div>

<hr>

<!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
<!-- <div class="row">
   <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h2><a href="https://debconf22.debconf.org/">DebConf22</a> on käynnissä!</h2>
      <p>Debian-kokoontuminen järjestetään Prizrenissä, Kosovossa sunnuntaista 17. päivä sunnunntaihin 24. päivä Heinäkuuta 2022.</p>
    </div>
  </div>
</div>-->

<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Uutiset ja julkistukset Debianiin liittyen</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="hankkeeseen liittyvät uutiset">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Kaikki uutiset</a> &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian News" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="security/dsa-long">
:#rss#}

