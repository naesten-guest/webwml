#use wml::debian::template title="Debian-Installer" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5d9ed11b1ac37b0f0417096ff186a5fd178b7fa6" maintainer="Lev Lamberov"

<h1>Новости</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Предыдущие новости</a>
</p>

<h1>Установка с помощью Debian-Installer</h1>

<p>
<if-stable-release release="bookworm">
<strong>Официальные установочные носители Debian <current_release_bookworm>, а также
информацию о нём</strong> ищите на
<a href="$(HOME)/releases/bookworm/debian-installer">странице bookworm</a>.
</if-stable-release>
<if-stable-release release="trixie">
<strong>Официальные установочные носители Debian <current_release_trixie>, а также
информацию о нём</strong> ищите на
<a href="$(HOME)/releases/trixie/debian-installer">странице trixie</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Все образы, перечисленные ниже, являются версиями Debian Installer, разрабатываемыми
для следующего выпуска Debian и установят по умолчанию тестируемую ветвь Debian
(<q><current_testing_name></q>).
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">
<p>

<strong>Для установки тестируемого выпуска Debian</strong> рекомендуется использовать
<strong>ежедневные сборки</strong> установщика. Доступны следующие
образы:

</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

<strong>Для установки тестируемой ветви Debian</strong> рекомендуем вам использовать программу
установки <strong>выпуска <humanversion /></strong>, после ознакомления со страницей
<a href="errata">известных ошибок</a>.
Для выпуска <humanversion /> доступны следующие образы:

</p>

<h2>Официальный выпуск</h2>

<div class="line">
<div class="item col50">
<strong>образы компакт-дисков netinst</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>образы компакт-дисков netinst (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>полные наборы компакт-дисков</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>полные наборы DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>полные наборы компакт-дисков (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>полные наборы DVD (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>полные наборы Blu-ray (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>другие образы (сетевой загрузки, USB-носители и т.п.)</strong>
<other-images />
</div>
</div>


<p>
Также вы можете использовать <b>срез</b> тестируемого дистрибутива Debian.
Еженедельно создаются полные работы образов, ежеднево создаются только
несколько основных образов.
</p>

<p>
Эти срезы устанавливают тестируемый выпуск Debian, но программа установки в них
основывается на нестабильном выпуске Debian.
</p>

</div>

<h2>Текущие недельные срезы</h2>

<div class="line">
<div class="item col50">
<strong>полный набор компакт-дисков</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>полный набор DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>полный набор компакт-дисков (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)
</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>полный набор DVD (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)
</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>полный набор Blu-ray (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)
</strong>
<devel-full-bd-jigdo />
</div>
</div>

</if-testing-installer>

<h2>Текущие ежедневные срезы</h2>

<div class="line">
<div class="item col50">
<strong>образы компакт-дисков netinst</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>образы компакт-дисков netinst (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>другие образы (сетевой загрузки, usb-диски и т.п.)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<strong>Замечания</strong>
</p>
<ul>
#	<li>Мы надеемся, что перед тем, как загрузить ежедневно генерируемые образы, вы ознакомились с
#	<a href="https://wiki.debian.org/DebianInstaller/Today">известными проблемами</a>;</li>
        <li>Ежедневно собираемые образы могут быть (временно) не для всех архитектур,
  	если ежедневная сборка не считается надёжной;</li>
	<li>Проверочные файлы (<tt>SHA512SUMS</tt> и <tt>SHA256SUMS</tt>) установочных
        образов доступны в том же каталоге, что и сами образы;</li>
	<li>Для загрузки полных образов компакт-дисков и дисков DVD
	рекомендуется использовать jigdo;</li>
        <li>Лишь ограниченное число образов из полных наборов DVD доступно
        в виде ISO файлов для прямой загрузки. Большинству пользователей не нужно всё ПО,
        доступное на всех этих дисках, поэтому, чтоб сохранить место на серверах и зеркалах,
        полные наборы образов доступны только через jigdo.</li>
</ul>

<p>
<strong>После использования Debian-Installer</strong> пришлите нам
<a href="https://d-i.debian.org/manual/ru.amd64/ch05s04.html#submit-bug">отчёт об установке</a>,
даже если не было никаких проблем.
</p>

<h1>Документация</h1>

<p>
<strong>Если вы хотите прочитать перед установкой только один документ</strong>, прочтите
<a href="https://d-i.debian.org/manual/ru.amd64/apa.html">Краткое руководство об установке</a>,
обзор процесса установки. Другая полезная документация:
</p>

<ul>
<li>Руководство по установке:
#   <a href="$(HOME)/releases/stable/installmanual">версия для текущего выпуска</a>
#   &mdash;
   <a href="$(HOME)/releases/testing/installmanual">для разрабатываемой версии (testing)</a>
   &mdash;
   <a href="https://d-i.debian.org/manual/">самая последняя версия (Git)</a>
<br />
подробные инструкции по установке</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ
по установщику Debian</a>
и <a href="$(HOME)/CD/faq/">FAQ по компакт-дискам Debian</a><br />
ответы на часто задаваемые вопросы</li>
<li><a href="https://wiki.debian.org/DebianInstaller">вики Debian-Installer</a><br />
документация, поддерживаемая сообществом</li>
</ul>

<h1>Как с нами связаться</h1>

<p>
<a href="https://lists.debian.org/debian-boot/">Список рассылки debian-boot</a>
является главным местом для обсуждения и координации работы над Debian-Installer.
</p>

<p>
У нас также есть канал IRC, #debian-boot на <tt>irc.debian.org</tt>. Этот
канал используется в основном для разработки, но иногда и для поддержки.
Если вы не получили в нём ответа, попробуйте написать в список рассылки.
</p>
