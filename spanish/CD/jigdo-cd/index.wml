#use wml::debian::cdimage title="Descargar imágenes de CD de Debian con jigdo" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="b0492ff8b5c976f85fec93aa66027d5218eff054" maintainer="Fernando C. Estrada"

<p>Jigsaw Download, o abreviadamente <a href="https://www.einval.com/~steve/software/jigdo/">jigdo
</a>, es una forma distribuir imágenes de CD/DVD de Debian que tiene en cuenta el ancho de banda disponible.</p>

<toc-display/>

<toc-add-entry name="why">¿Por qué jigdo es mejor que una descarga
directa?</toc-add-entry>

<p>¡Porque es más rápido! Por varias razones, hay muchas menos réplicas para
imágenes de CD/DVD que para el archivo «normal» de Debian. Consecuentemente,
si descarga desde una réplica de imágenes de CD, esa réplica no sólo
estará más lejos de su ubicación, además estará sobrecargada,
especialmente justo después de una publicación.</p>

<p>Además, algunos tipos de imágenes no están disponibles para descarga 
completa como <tt>.iso</tt> porque no hay suficiente espacio en nuestros
servidores para alojarlas.</p>

<p>Por supuesto, una réplica de Debian «normal» no lleva ninguna
imagen de CD/DVD, entonces ¿cómo las descarga jigdo de allí? jigdo lo logra
descargando individualmente todos los ficheros que están en el CD/DVD. En el
siguiente paso, todos estos ficheros se ensamblan en uno más grande
que es una copia exacta de la imagen del CD/DVD. Sin embargo, todo esto
ocurre entre bambalinas. Todo lo que <em>usted</em> tiene que hacer es
decirle a la herramienta de descarga la localización de un fichero
«<tt>.jigdo</tt>» para procesarlo.</p>

<p>Dispone de más información en la
<a href="https://www.einval.com/~steve/software/jigdo/">página de jigdo</a>.
¡Los voluntarios que quieran ayudar a
desarrollar jigdo siempre serán bienvenidos!</p>

<toc-add-entry name="how">¿Cómo se descarga una imagen con jigdo?</toc-add-entry>

<ul>

  <li>Descargue un paquete que contenga <tt>jigdo-lite</tt>, que está
  disponible para instalación directa en las distribuciones Debian y Ubuntu
  en el paquete <tt>jigdo-file</tt>. Para FreeBSD,
  instale desde /usr/ports/net-p2p/jigdo o bien obtenga el paquete
  con <tt>pkg_add -r jigdo</tt>. Para otras opciones de instalación
  (binarios para Windows, o código fuente), visite 
  la <a href="https://www.einval.com/~steve/software/jigdo/">página de jigdo</a>.</li>

  <li>Ejecute el script <tt>jigdo-lite</tt>. Le pedirá la URL de un
  fichero «<tt>.jigdo</tt>» para procesar. (Puede proporcionar una URL en
  la línea de órdenes si así lo desea).</li>

  <li>Escoja los ficheros «<tt>.jigdo</tt>» que desee descargar de uno de
  los lugares indicados <a href="#which">más adelante</a>, e introduzca
  las URLs en el indicador de órdenes de <tt>jigdo-lite</tt>. Cada fichero
  «<tt>.jigdo</tt>» se corresponde a una imagen «<tt>.iso</tt>» de CD o
  DVD.</li>

  <li>Si lo usa por primera vez, limítese a pulsar Intro cuando se
  le pregunte «Files to scan».</li>

  <li>A la pregunta «Debian mirror», teclee
  <kbd>http://deb.debian.org/debian/</kbd> o bien 
  <kbd>http://ftp.<strong><var>XY</var></strong>.debian.org/debian/</kbd>,
  donde <strong><var>XY</var></strong> es el código de dos letras de su
  país (por ejemplo, <tt>us</tt>, <tt>de</tt>, <tt>uk</tt>. Consulte la
  lista actualizada de <a href="$(HOME)/mirror/list">localizaciones
  ftp.<var>XY</var>.debian.org disponibles</a>.)</li>

  <li>Siga las instrucciones que indique el script. Si todo va bien, el
  script finaliza calculando una suma de comprobación de la imagen
  generada, y diciéndole que la suma coincide con la de la imagen original.</li>

</ul>

<p>Para una descripción detallada, paso a paso de este proceso,
eche un vistazo al <a href="https://tldp.org/HOWTO/Debian-Jigdo/">
Debian jigdo mini-COMO</a>. Este COMO explica también las características
avanzadas de jigdo, como actualización de versiones antiguas de imágenes de
CD/DVD a la versión actual (descargando sólo lo que cambió, no la imagen entera).</p>

<p>Una vez que haya descargado las imágenes y las haya escrito en CD,
asegúrese de leer la <a
href="$(HOME)/releases/stable/installmanual">información detallada sobre
el proceso de instalación</a>.</p>

<toc-add-entry name="which">Imágenes oficiales</toc-add-entry>

 <h3>Archivos oficiales de jigdo para la publicación <q>estable</q></h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-jigdo />
</div>
<div class="clear"></div>
</div>
<div class="line">
<div class="item col50">
<p><strong>Blu-ray</strong></p>
  <stable-full-bluray-jigdo />
</div>
</div>

<p>Asegúrese de echarle un vistazo a la documentación antes de instalar.
<strong>Si sólo va a leer un documento</strong> antes de instalar, lea nuestro
<a href="$(HOME)/releases/stable/amd64/apa.es">Cómo instalar</a>, un paseo rápido
por el proceso de instalación. Entre otra documentación útil están:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guía de instalación</a>,
    las instrucciones de instalación detalladas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentación del instalador de Debian</a>, 
incluyendo las respuestas a preguntas frecuentes</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Erratas del instalador de Debian</a>,
    la lista de problemas conocidos en el instalador</li>
</ul>

 <h3>Archivos oficiales de jigdo para la publicación «en pruebas»:</h3>
<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <devel-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <devel-full-dvd-jigdo />
</div>
</div>

<hr />

<toc-add-entry name="search">Búsqueda de contenidos en imágenes de CD</toc-add-entry>

<p><strong>¿Qué imagen de CD/DVD contiene un cierto fichero?</strong> Más 
adelante, puede buscar las listas de ficheros contenidos en una gran
variedad de imágenes de CD/DVD. Puede introducir varias palabras, cada palabra debe coincidir
con una subcadena del nombre del fichero. Añada p.e. «_i386» para restringir
los resultados a una cierta arquitectura. Añada «_all» para ver paquete que son
idénticos para todas las arquitecturas.</p>

<form method="get" action="https://cdimage-search.debian.org/"><p>
<input type="hidden" name="search_area" value="release">
<input type="hidden" name="type" value="simple">
<input type="text" name="query" size="20" value="">
# Traductores: "Search" es traducible
<input type="submit" value="Buscar"></p></form>

<p><strong>¿Qué ficheros contiene una cierta imagen?</strong> Si 
necesita una lista de <em>todos</em> los ficheros que contiene un cierto
CD/DVD de Debian, sólo busque el fichero <tt>list.gz</tt> de acuerdo a
la imagen correspondiente en <a
href="https://cdimage.debian.org/cdimage/">cdimage.debian.org</a>.</p>

<hr>

<toc-add-entry name="faq">Preguntas realizadas/respondidas con
frecuencia</toc-add-entry>

<p><strong>¿Cómo hago que jigdo use mi proxy?</strong></p>

<p>Abra el fichero <tt>~/.jigdo-lite</tt> (o
<tt>jigdo-lite-settings.txt</tt> para la versión de Windows) con un editor
de texto y encuentre la línea que comienza por 'wgetOpts'. Ahí puede
añadir las siguientes opciones:</p>

<p><tt>-e ftp_proxy=http://<i>LOCAL-PROXY</i>:<i>PUERTO</i>/</tt>
<br><tt>-e http_proxy=http://<i>LOCAL-PROXY</i>:<i>PUERTO</i>/</tt>
<br><tt>--proxy-user=<i>USUARIO</i></tt>
<br><tt>--proxy-passwd=<i>CLAVE</i></tt></p>

<p>Poniendo los valores correctos para su servidor proxy, claro. Las
últimas dos opciones sólo son necesarias si su proxy usa autenticación
mediante contraseña. Debe añadir las opciones al final de la línea de wgetOpts
<em>antes</em> del último carácter <tt>'</tt>. Todas las opciones deben
estar en la misma línea.</p>

<p>En Linux también puede configurar, de forma alternativa, las variables
de entorno <tt>ftp_proxy</tt> y <tt>http_proxy</tt>, por ejemplo en el
fichero <tt>/etc/environment</tt> o en <tt>~/.bashrc</tt>.</p>

<p><strong>¡Aargh! El script falló por un error. ¿He descargado en vano
todos esos MBs?</strong></p>

<p>Por supuesto que esto No Debería Ocurrir (tm), pero por varios motivos
puede acabar en un estado donde ya se haya generado un archivo grande
«<tt>.iso.tmp</tt>» y <tt>jigdo-lite</tt> parece tener problemas,
diciéndole repetidamente que intente reiniciar la descarga. Hay algunas
cosas que puede intentar en estos casos:</p>

<ul>

  <li>Simplemente reinicie la descarga pulsando «Intro». Puede que
  algunos de los ficheros no pudieran descargarse debido a que se excedió
  el tiempo de espera o por otros errores pasajeros. El programa intentará otra vez
  descargar todos los archivos perdidos.</li>

  <li>Pruebe una réplica diferente. Algunas réplicas de Debian están
  ligeramente desincronizadas. Puede que una réplica diferente
  tenga los archivos que fueron borrados de la que indicó, o ya se
  actualizó con ficheros que aún no están presentes en su réplica.</li>

  <li>Recupere las partes perdidas de la imagen usando <tt><a
  href="https://rsync.samba.org/">rsync</a></tt>. Primero, necesita
  encontrar la URL correcta para rsync de la imagen que está descargando:
  Seleccione un servidor que ofrezca acceso rsync a las imágenes <a
  href="../mirroring/rsync-mirrors">estable</a> o <a
  href="../http-ftp/#testing">en pruebas</a>, luego determine la ruta
  correcta y el nombre del fichero. Se puede obtener el listado del
  directorio con comandos como
  <tt>rsync&nbsp;rsync://cdimage.debian.org/debian-cd/</tt>

  <br>Lo siguiente es borrar la extensión «<tt>.tmp</tt>» del fichero temporal
  de <tt>jigdo-lite</tt> renombrándolo, y pasar ambos (la URL remota y
  el nombre del fichero local) a rsync:
  <tt>rsync&nbsp;rsync://server.org/path/binary-i386-1.iso
  binary-i386-1.iso</tt>

  <br>Puede que quiera usar las opciones de rsync <tt>--verbose</tt> y
  <tt>--progress</tt> para obtener mensajes de estado, y
  <tt>--block-size=8192</tt> para incrementar su velocidad.</li>
  
  <li>Si todo lo demás falla, no habrá perdido los datos de su descarga:
  Si usa Linux puede montar, en modo dispositivo de bucle («loop»), el fichero
  <tt>.tmp</tt> para acceder a los paquetes que ya se descargaron, y
  reutilizarlos para generar una imagen partiendo de un fichero jigdo más
  nuevo (por ejemplo, la «captura» semanal de la distribución «en pruebas» si la
  descarga que falló también era de en pruebas). Para hacerlo, ejecute estas
  órdenes siendo superusuario, en el directorio de la descarga fallida:
  <tt>mkdir&nbsp;mnt;  mount&nbsp;-t&nbsp;iso9660&nbsp;-o&nbsp;loop&nbsp;*.tmp&nbsp;mnt</tt>
  Lo siguiente, es iniciar una nueva descarga en un directorio diferente, e
  introducir la ruta del directorio <tt>mnt</tt> a la pregunta
  «Files to scan» (ficheros para examinar).</li>

</ul>
